-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2017 at 07:27 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `census`
--
DROP DATABASE census;
CREATE DATABASE census CHARACTER SET utf8 COLLATE utf8_general_ci;
USE census;

-- Created Table  --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `activity` (
  `activityId` tinyint(2) NOT NULL AUTO_INCREMENT,
  `aName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`activityId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `address` (
`addressId` int(10) NOT NULL AUTO_INCREMENT,
  `road` varchar(100) DEFAULT NULL,
  `alley` varchar(100) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `aNum` varchar(10) DEFAULT NULL,
   PRIMARY KEY (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `area` (
`areaId` int(5) NOT NULL AUTO_INCREMENT,
  `areaCode` tinyint(1) NOT NULL,
  `areaNumber` varchar(10) NOT NULL,
  `areaName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`areaId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `canton` (
`cantonId` int(4) NOT NULL AUTO_INCREMENT,
  `cantonCode` varchar(6) NOT NULL,
  `cantonName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cantonId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company` (
`comId` int(10) NOT NULL AUTO_INCREMENT,
  `titleName` varchar(50) DEFAULT NULL,
  `fName` varchar(50) DEFAULT NULL,
  `lName` varchar(50) DEFAULT NULL,
  `cName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`comId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `district` (
`districtId` tinyint(4) NOT NULL AUTO_INCREMENT,
  `districtCode` varchar(2) NOT NULL,
  `districtName` varchar(50) DEFAULT NULL,
  `provinceCode` varchar(4) NOT NULL,
   PRIMARY KEY (`districtId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `formateconomy` (
  `feId` tinyint(2) NOT NULL AUTO_INCREMENT,
  `feName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`feId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `formatlaw` (
  `flId` tinyint(2) NOT NULL AUTO_INCREMENT,
  `flName` varchar(50) DEFAULT NULL,
   PRIMARY KEY (`flId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hotel` (
`hId` int(5) NOT NULL AUTO_INCREMENT,
  `hType` tinyint(1) NOT NULL,
  `hRoom` int(4) DEFAULT NULL,
  `hMaxPrice` int(6) DEFAULT NULL,
  `hMinPrice` int(6) DEFAULT NULL,
  PRIMARY KEY (`hId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `province` (
`provinceId` tinyint(2) NOT NULL AUTO_INCREMENT,
  `provinceCode` varchar(2) NOT NULL,
  `provinceName` varchar(50) DEFAULT NULL,
  `regionCode` varchar(4) NOT NULL,
  PRIMARY KEY (`provinceId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `region` (
`regionId` tinyint(2) NOT NULL AUTO_INCREMENT,
  `regionCode` varchar(2) NOT NULL,
  `regionName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`regionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `status` (
  `statusId` tinyint(2) NOT NULL AUTO_INCREMENT,
  `sName` varchar(50) DEFAULT NULL,
   PRIMARY KEY (`statusId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `tsic` (
`tsId` int(10) NOT NULL AUTO_INCREMENT,
  `tsic` int(5) NOT NULL,
  `isic` int(5) DEFAULT NULL,
  `tsName` varchar(255) NOT NULL,
  `ActivityId` tinyint(2) NOT NULL,
  PRIMARY KEY (`tsId`) USING BTREE,
  KEY `TSIC_KEY` (`tsic`),
  UNIQUE KEY `isic` (`isic`),
  KEY `TSIC_FK1` (`ActivityId`),
  CONSTRAINT `TSIC_FK1` FOREIGN KEY (`ActivityId`) REFERENCES `activity` (`activityId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `users` (
`usersId` int(10) NOT NULL AUTO_INCREMENT,
  `userName` varchar(8) NOT NULL,
  `password` varchar(255) NOT NULL,
  `prefix` int(2) NOT NULL,
  `fName` varchar(50) NOT NULL,
  `lName` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `trId` tinyint(1) NOT NULL,
  PRIMARY KEY (`usersId`, `userName`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `prefixs` (
`pId` int(11) NOT NULL,
  `pName` varchar(10) NOT NULL,
  PRIMARY KEY (`pId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `village` (
`villageId` int(10) NOT NULL AUTO_INCREMENT,
  `EACode` varchar(13) NOT NULL,
  `villageNumber` tinyint(2) NOT NULL,
  `villageName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`villageId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mainbranch` (
`mbId` int(4) NOT NULL AUTO_INCREMENT,
  `road` varchar(100) DEFAULT NULL,
  `alley` varchar(100) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `aNum` int(10) DEFAULT NULL,
  `provinceCode` varchar(2) NOT NULL DEFAULT '0',
  `districtCode` varchar(2) NOT NULL DEFAULT '0',
  `cantonCode` varchar(2) NOT NULL DEFAULT '0',
  `CompanyId` int(10) NOT NULL,
  PRIMARY KEY (`mbId`),
  KEY `MainBranch_FK1` (`CompanyId`),
  CONSTRAINT `MainBranch_FK1` FOREIGN KEY (`CompanyId`) REFERENCES `company` (`comId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `census` (
  `censusId` int(10) NOT NULL AUTO_INCREMENT,
  `dateKey` date NOT NULL,
  `dateUpdate` date NULL DEFAULT NULL,
  `yearCreate` varchar(4) NOT NULL,
  `regionCode` varchar(2) NOT NULL,
  `provinceCode` varchar(2) NOT NULL,
  `districtCode` varchar(2) NOT NULL,
  `cantonCode` varchar(2) NOT NULL,
  `areaCode` varchar(1) NOT NULL,
  `areaNumber` varchar(3) NOT NULL,
  `eaCode` varchar(3) NOT NULL,
  `censusCode` varchar(15) NOT NULL,
  `no` varchar(3) NOT NULL,
  `otherStatus` VARCHAR(100) DEFAULT NULL,
  `typeActivity` varchar(100) DEFAULT NULL,
  `otherFEName` varchar(50) DEFAULT NULL,
  `otherFLName` varchar(50) DEFAULT NULL,
  `RegisTypeId` tinyint(1) DEFAULT NULL,
  `personalId` varchar(13) DEFAULT NULL,
  `nationPartnerId` tinyint(1) DEFAULT NULL,
  `totalWorkers` int(5) DEFAULT NULL,
  `workers` int(5) DEFAULT NULL,
  `yearActivity` tinyint(3) DEFAULT NULL,
  `hospitalBed` int(4) DEFAULT NULL,
  `useComputerId` tinyint(1) DEFAULT 3,
  `useInternetId` tinyint(1) DEFAULT 2,
  `saleOwnWeb` tinyint(1) DEFAULT NULL,
  `saleOtherWeb` tinyint(1) DEFAULT NULL,
  `saleEmarket` tinyint(1) DEFAULT NULL,
  `saleSocial` tinyint(1) DEFAULT NULL,
  `notSale` tinyint(1) DEFAULT NULL,
  `tel` varchar(10) DEFAULT NULL,
  `web` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `AddressId` int(10) NOT NULL,
  `CompanyId` int(10) NOT NULL,
  `StatusId` tinyint(2) NOT NULL,
  `ActivityId` tinyint(2) DEFAULT NULL,
  `FormatLawId` tinyint(2) DEFAULT NULL,
  `FormatEconomyId` tinyint(2) DEFAULT NULL,
  `HotelId` int(5) DEFAULT NULL,
  `TSICId` int(10) DEFAULT NULL,
  `MainBranchId` int(4) DEFAULT NULL,
  `operatorId` varchar(8) NOT NULL COMMENT 'ผู้ปฏิบัติงาน',
  `dateOperator` date NOT NULL,
  `editorialStaffId` varchar(8) NOT NULL COMMENT 'เจ้าหน้าที่บรรณาธิการ',
  `dateEditorialStaff` date NOT NULL,
  `recorderStaffId` varchar(8) NOT NULL COMMENT 'เจ้าหน้าที่บันทึกข้อมูล',
  `dateRecorderStaff` date NOT NULL,
  `examinerId` varchar(8) NOT NULL COMMENT 'ผู้ตรวจ',
  `dateExaminer` date NOT NULL,
  PRIMARY KEY (`censusId`),
  KEY `CENSUS_FK1` (`HotelId`),
  KEY `CENSUS_FK2` (`StatusId`),
  KEY `CENSUS_FK3` (`ActivityId`),
  KEY `CENSUS_FK4` (`FormatEconomyId`),
  KEY `CENSUS_FK5` (`AddressId`),
  KEY `CENSUS_FK6` (`FormatLawId`),
  KEY `CENSUS_FK7` (`MainBranchId`),
  KEY `CENSUS_FK8` (`CompanyId`),
  KEY `CENSUS_FK9` (`TSICId`),
  KEY `CENSUS_FK10` (`operatorId`),
  KEY `CENSUS_FK11` (`editorialStaffId`),
  KEY `CENSUS_FK12` (`recorderStaffId`),
  KEY `CENSUS_FK13` (`examinerId`),
  CONSTRAINT `CENSUS_FK1` FOREIGN KEY (`HotelId`) REFERENCES `hotel` (`hId`),
  CONSTRAINT `CENSUS_FK2` FOREIGN KEY (`StatusId`) REFERENCES `status` (`statusId`),
  CONSTRAINT `CENSUS_FK3` FOREIGN KEY (`ActivityId`) REFERENCES `activity` (`activityId`),
  CONSTRAINT `CENSUS_FK4` FOREIGN KEY (`FormatEconomyId`) REFERENCES `formateconomy` (`feId`),
  CONSTRAINT `CENSUS_FK5` FOREIGN KEY (`AddressId`) REFERENCES `address` (`addressId`),
  CONSTRAINT `CENSUS_FK6` FOREIGN KEY (`FormatLawId`) REFERENCES `formatlaw` (`flId`),
  CONSTRAINT `CENSUS_FK7` FOREIGN KEY (`MainBranchId`) REFERENCES `mainbranch` (`mbId`),
  CONSTRAINT `CENSUS_FK8` FOREIGN KEY (`CompanyId`) REFERENCES `company` (`comId`),
  CONSTRAINT `CENSUS_FK9` FOREIGN KEY (`TSICId`) REFERENCES `tsic` (`tsic`),
  CONSTRAINT `CENSUS_FK10` FOREIGN KEY (`operatorId`) REFERENCES `users` (`userName`),
  CONSTRAINT `CENSUS_FK11` FOREIGN KEY (`editorialStaffId`) REFERENCES `users` (`userName`),
  CONSTRAINT `CENSUS_FK12` FOREIGN KEY (`recorderStaffId`) REFERENCES `users` (`userName`),
  CONSTRAINT `CENSUS_FK13` FOREIGN KEY (`examinerId`) REFERENCES `users` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Add into Table ---------------------------------------------------------------------------
INSERT INTO `area` (`areaId`, `areaCode`, `areaNumber`, `areaName`) VALUES
(1, 1, '1901011099', 'เทศบาลเมืองสระบุรี'),
(2, 2, '1901052001', 'อบต. ดาวเรือง'),
(3, 2, '1901062001', 'อบต. ดาวเรือง'),
(4, 2, '1901072002', 'อบต. โคกสว่าง'),
(5, 1, '1901081095', 'เทศบาลป๊อกแป๊ก'),
(6, 2, '1901092003', 'อบต. หนองโน'),
(7, 2, '1901092004', 'อบต. หนองยาว'),
(8, 2, '1901102005', 'อบต. ปากข้าวสาร'),
(9, 2, '1901112006', 'อบต. หนองปลาไหล'),
(10, 1, '1901121078', 'เทศบาลตำบลกุดนกเปล้า'),
(11, 2, '1901132007', 'อบต. ตลิ่งชัน'),
(12, 1, '1901141077', 'เทศบาลตำบลตะกุด'),
(13, 1, '1902011098', 'เทศบาลเมืองแก่งคอย'),
(14, 1, '1902021094', 'เทศบาลเมืองทับกวาง'),
(15, 2, '1902032001', 'อบต. ตาลเดี่ยว'),
(16, 2, '1902042002', 'อบต. ห้วยแห้ง'),
(17, 2, '1902052003', 'อบต. ท่าคล้อ'),
(18, 2, '1902062004', 'อบต. หินช้อน'),
(19, 2, '1902072005', 'อบต. ท่าตูม'),
(20, 2, '1902082006', 'อบต. บ้านป่า'),
(21, 2, '1902092006', 'อบต. ท่าตูม'),
(22, 2, '1902102007', 'อบต. ชะอม'),
(23, 2, '1902112008', 'อบต. สองคอน'),
(24, 2, '1902122009', 'อบต. เตาปูน'),
(25, 2, '1902132010', 'อบต. ชำผักแพว'),
(26, 2, '1902152011', 'อบต. ท่ามะปราง'),
(27, 1, '1903011097', 'เทศบาลตำบลหนองแค'),
(28, 2, '1903022001', 'อบต. กุ่มหัก'),
(29, 1, '1903031093', 'เทศบาลตำบลคชสิทธิ์'),
(30, 2, '1903032002', 'อบต. คชสิทธิ์'),
(31, 2, '1903042003', 'อบต. โคกตูม'),
(32, 2, '1903052004', 'อบต. โคกแย้'),
(33, 2, '1903062005', 'อบต. บัวลอย'),
(34, 1, '1903071076', 'เทศบาลตำบลไผ่ต่ำ'),
(35, 2, '1903082003', 'อบต. โคกตูม'),
(36, 1, '1903091092', 'เทศบาลตำบลหินกอง'),
(37, 2, '1903092006', 'อบต. ห้วยขมิ้น'),
(38, 1, '1903101092', 'เทศบาลตำบลหินกอง'),
(39, 2, '1903102007', 'อบต. ห้วยทราย'),
(40, 2, '1903102007', 'อบต. ห้วยทราย'),
(41, 1, '1903111092', 'เทศบาลตำบลหินกอง'),
(42, 2, '1903112008', 'อบต. หนองไข่น้ำ'),
(43, 2, '1903122009', 'อบต. หนองแขม'),
(44, 2, '1903132010', 'อบต. หนองจิก'),
(45, 2, '1903142011', 'อบต. หนองจรเข้'),
(46, 2, '1903152012', 'อบต. หนองนาก'),
(47, 2, '1903162013', 'อบต. หนองปลาหมอ'),
(48, 2, '1903172014', 'อบต. หนองปลิง'),
(49, 2, '1903182015', 'อบต. หนองโรง'),
(50, 1, '1904011090', 'เทศบาลตำบลหนองหมู'),
(51, 2, '1904012001', 'อบต. หนองหมู'),
(52, 1, '1904021091', 'เทศบาลตำบลวิหารแดง'),
(53, 2, '1904022002', 'อบต. บ้านลำ'),
(54, 1, '1904031091', 'เทศบาลตำบลวิหารแดง'),
(55, 2, '1904032003', 'อบต. คลองเรือ'),
(56, 1, '1904041091', 'เทศบาลตำบลวิหารแดง'),
(57, 1, '1904041090', 'เทศบาลตำบลหนองหมู'),
(58, 0, '190402004', 'อบต. วิหารแดง'),
(59, 0, '190401091', 'เทศบาลตำบลวิหารแดง'),
(60, 1, '1904051090', 'เทศบาลตำบลหนองหมู'),
(61, 2, '1904052005', 'อบต. หนองสรวง'),
(62, 1, '1904061091', 'เทศบาลตำบลวิหารแดง'),
(63, 2, '1904062006', 'อบต. เจริญธรรม'),
(64, 1, '1905011089', 'เทศบาลตำบลหนองแซง'),
(65, 1, '1905021089', 'เทศบาลตำบลหนองแซง'),
(66, 2, '1905022001', 'อบต. หนองกบ'),
(67, 2, '1905032002', 'อบต. หนองหัวโพ'),
(68, 2, '1905042002', 'อบต. หนองหัวโพ'),
(69, 2, '1905052001', 'อบต. หนองกบ'),
(70, 1, '1905061089', 'เทศบาลตำบลหนองแซง'),
(71, 2, '1905062003', 'อบต. ไก่เส่า'),
(72, 2, '1905072004', 'อบต. โคกสะอาด'),
(73, 2, '1905082005', 'อบต. ม่วงหวาน'),
(74, 2, '1905092005', 'อบต. ม่วงหวาน'),
(75, 1, '1906011087', 'เทศบาลตำบลบ้านหมอ'),
(76, 2, '1906012001', 'อบต. เมืองขีดขิน'),
(77, 1, '1906021069', 'เทศบาลตำบลบางโขมด'),
(78, 1, '1906031068', 'เทศบาลตำบลบางโขมด'),
(79, 1, '1906041072', 'เทศบาลตำบลตลาดน้อย'),
(80, 2, '1906052005', 'อบต. โคกใหญ่'),
(81, 2, '1906062005', 'อบต. โคกใหญ่'),
(82, 2, '1906072006', 'อบต. ไผ่ขวาง'),
(83, 1, '1906081088', 'เทศบาลตำบลท่าลาน'),
(84, 1, '1906091063', 'เทศบาลตำบลหนองบัว'),
(85, 1, '1907011086', 'เทศบาลตำบลดอนพุด'),
(86, 1, '1907021086', 'เทศบาลตำบลดอนพุด'),
(87, 1, '1907031086', 'เทศบาลตำบลดอนพุด'),
(88, 2, '1907042001', 'อบต. ดงตะงาว'),
(89, 1, '1908011085', 'อบต. ดงตะงาว'),
(90, 2, '1908012001', 'อบต. หนองโคน'),
(91, 2, '1908022002', 'อบต. บ้านกลับ'),
(92, 2, '1908032003', 'อบต. ดอนทอง'),
(93, 2, '1908042002', 'อบต. บ้านกลับ'),
(94, 1, '1909011096', 'เทศบาลตำบลพระพุทธบาท'),
(95, 1, '1909021096', 'เทศบาลตำบลพระพุทธบาท'),
(96, 1, '1909031096', 'เทศบาลตำบลพระพุทธบาท'),
(97, 1, '1909031067', 'เทศบาลตำบลธารเกษม'),
(98, 1, '1909031067', 'เทศบาลตำบลธารเกษม'),
(99, 1, '1909041062', 'เทศบาลตำบลนายาว'),
(100, 1, '1909051096', 'เทศบาลตำบลพระพุทธบาท'),
(101, 2, '1909052003', 'อบต. พุคำจาน'),
(102, 2, '1909062004', 'อบต. เขาวง'),
(103, 1, '1909071064', 'เทศบาลตำบลห้วยป่าหวาย'),
(104, 1, '1909081074', 'เทศบาลตำบลพุกร่าง'),
(105, 1, '1909091065', 'เทศบาลตำบลหนองแก'),
(106, 1, '1910011082', 'เทศบาลตำบลเสาให้'),
(107, 1, '1910021084', 'เทศบาลตำบลบ้านยาง'),
(108, 2, '1910022001', 'อบต. บ้านยาง'),
(109, 1, '1910031070', 'เทศบาลตำบลหัวปลวก'),
(110, 2, '1910042003', 'อบต. ช้างไทยงาม'),
(111, 2, '1910052003', 'อบต. ช้างไทยงาม'),
(112, 1, '1910061071', 'เทศบาลตำบลต้านตาล-พระยาท'),
(113, 2, '1910072003', 'อบต. ช้างไทยงาม'),
(114, 1, '1910081071', 'อบต. ช้างไทยงาม'),
(115, 2, '1910092005', 'อบต. ม่วงงาม'),
(116, 2, '1910102006', 'อบต. เริงราง'),
(117, 1, '1910111073', 'เทศบาลตำบลเมืองเก่า'),
(118, 1, '1910121083', 'เทศบาลตำบลสวนดอกไม้'),
(119, 1, '1911011081', 'เทศบาลตำบลสวนมวกเหล็ก'),
(120, 2, '1911012001', 'อบต. มวกเหล็ก'),
(121, 1, '1911021081', 'เทศบาลตำบลสวนมวกเหล็ก'),
(122, 2, '1911022002', 'อบต. มิตรภาพ'),
(123, 2, '1911042003', 'อบต. หนองย่างเสือ'),
(124, 2, '1911052004', 'อบต. ลำสมพุง'),
(125, 2, '1911072005', 'อบต. ลำพญากลาง'),
(126, 2, '1911092006', 'อบต. ซับสนุ่น'),
(127, 1, '1912011066', 'เทศบาลตำบลแสลงพัน'),
(128, 1, '1912021080', 'เทศบาลตำบลวังม่วง'),
(129, 1, '1912021075', 'เทศบาลตำบลคำพราน'),
(130, 1, '1912031080', 'เทศบาลตำบลวังม่วง'),
(131, 2, '1912032002', 'อบต. วังม่วง'),
(132, 2, '1913012001', 'อบต. เขาดินพัฒนา'),
(133, 2, '1913022002', 'อบต. บ้านแก้ง'),
(134, 2, '1913032003', 'อบต. ผึ้งรวง'),
(135, 2, '1913042004', 'อบต. พุแค'),
(136, 2, '1913052005', 'อบต. ห้วยบง'),
(137, 1, '1913061079', 'เทศบาลตำบลหน้าพระลาน'),
(138, 2, '1913062006', 'อบต. หน้าพระลาน');

INSERT INTO `canton` (`cantonId`, `cantonCode`, `cantonName`) VALUES
(1, '190101', 'ปากเพรียว'),
(2, '190105', 'ดาวเรือง'),
(3, '190106', 'นาโฉง'),
(4, '190107', 'โคกสว่าง'),
(5, '190108', 'หนองโน'),
(6, '190109', 'หนองยาว'),
(7, '190110', 'ปากข้าวสาร'),
(8, '190111', 'หนองปลาไหล'),
(9, '190112', 'กุดนกเปล้า'),
(10, '190113', 'ตลิ่งชัน'),
(11, '190114', 'ตะกุด'),
(12, '190201', 'แก่งคอย'),
(13, '190202', 'ทับกวาง'),
(14, '190203', 'ตาลเดี่ยว'),
(15, '190204', 'ห้วยแห้ง'),
(16, '190205', 'ท่าคล้อ'),
(17, '190206', 'หินซ้อน'),
(18, '190207', 'บ้านธาตุ'),
(19, '190208', 'บ้านป่า'),
(20, '190209', 'ท่าตูม'),
(21, '190210', 'ชะอม'),
(22, '190211', 'สองคอน'),
(23, '190212', 'เตาปูน'),
(24, '190213', 'ชำผักแพว'),
(25, '190215', 'ท่ามะปราง'),
(26, '190301', 'หนองแค'),
(27, '190302', 'กุ่มหัก'),
(28, '190303', 'คชสิทธิ์'),
(29, '190304', 'โคกตูม'),
(30, '190305', 'โคกแย้'),
(31, '190306', 'บัวลอย'),
(32, '190307', 'ไผ่ต่ำ'),
(33, '190308', 'โพนทอง'),
(34, '190309', 'ห้วยขมิ้น'),
(35, '190310', 'ห้วยทราย'),
(36, '190311', 'หนองไข่น้ำ'),
(37, '190312', 'หนองแขม'),
(38, '190313', 'หนองจิก'),
(39, '190314', 'หนองจรเข้'),
(40, '190315', 'หนองนาก'),
(41, '190316', 'หนองปลาหมอ'),
(42, '190317', 'หนองปลิง'),
(43, '190318', 'หนองโรง'),
(44, '190401', 'หนองหมู'),
(45, '190402', 'บ้านลำ'),
(46, '190403', 'คลองเรือ'),
(47, '190404', 'วิหารแดง'),
(48, '190405', 'หนองสรวง'),
(49, '190406', 'เจริญธรรม'),
(50, '190501', 'หนองแซง'),
(51, '190502', 'หนองควายโซ'),
(52, '190503', 'หนองหัวโพ'),
(53, '190504', 'หนองสีดา'),
(54, '190505', 'หนองกบ'),
(55, '190506', 'ไก่เส่า'),
(56, '190507', 'โคกสะอาด'),
(57, '190508', 'ม่วงหวาน'),
(58, '190601', 'บ้านหมอ'),
(59, '190602', 'บางโขมด'),
(60, '190603', 'สร้างโศก'),
(61, '190604', 'ตลาดน้อย'),
(62, '190605', 'หรเทพ'),
(63, '190606', 'โคกใหญ่'),
(64, '190607', 'ไผ่ขวาง'),
(65, '190608', 'บ้านครัว'),
(66, '190609', 'หนองบัว'),
(67, '190701', 'ดอนพุด'),
(68, '190702', 'ไผ่หลิ่ว'),
(69, '190703', 'บ้านหลวง'),
(70, '190704', 'ดงตะงาว'),
(71, '190801', 'หนองโดน'),
(72, '190802', 'บ้านกลับ'),
(73, '190803', 'ดอนทอง'),
(74, '190804', 'บ้านโปร่ง'),
(75, '190901', 'พระพุทธบาท'),
(76, '190902', 'ขุนโขลน'),
(77, '190903', 'ธารเกษม'),
(78, '190904', 'นายาว'),
(79, '190905', 'พุคำจาน'),
(80, '190906', 'เขาวง'),
(81, '190907', 'ห้วยป่าหวาย'),
(82, '190908', 'พุกร่าง'),
(83, '191001', 'เสาไห้'),
(84, '191002', 'บ้านยาง'),
(85, '191003', 'หัวปลวก'),
(86, '191004', 'งิ้วงาม'),
(87, '191005', 'ศาลารีไทย'),
(88, '191006', 'ต้นตาล'),
(89, '191007', 'ท่าช้าง'),
(90, '191008', 'พระยาทด'),
(91, '191009', 'ม่วงงาม'),
(92, '191010', 'เริงราง'),
(93, '191011', 'เมืองเก่า'),
(94, '191012', 'สวนดอกไม้'),
(95, '191101', 'มวกเหล็ก'),
(96, '191102', 'มิตรภาพ'),
(97, '191104', 'หนองย่างเสือ'),
(98, '191105', 'ลำสมพุง'),
(99, '191107', 'ลำพญากลาง'),
(100, '191109', 'ซับสนุ่น'),
(101, '191201', 'แสลงพัน'),
(102, '191202', 'คำพราน'),
(103, '191203', 'วังม่วง'),
(104, '191301', 'เขาดินพัฒนา'),
(105, '191302', 'บ้านแก้ง'),
(106, '191303', 'ผึ้งรวง'),
(107, '191304', 'พุแค'),
(108, '191305', 'ห้วยบง'),
(109, '191306', 'หน้าพระลาน');

INSERT INTO `province` (`provinceId`, `provinceCode`, `provinceName`, `regionCode`) VALUES
(1, 19, 'สระบุรี', 2);

INSERT INTO `district` (`districtId`, `districtCode`, `districtName`, `provinceCode`) VALUES
(4, 1, 'เมืองสระบุรี', 19),
(5, 2, 'แก่งคอย', 19),
(6, 3, 'หนองแค', 19),
(7, 4, 'วิหารแดง', 19),
(8, 5, 'หนองแซง', 19),
(9, 6, 'บ้านหมอ', 19),
(10, 7, 'ดอนพุด', 19),
(11, 8, 'หนองโดน', 19),
(12, 9, 'พระพุทธบาท', 19),
(13, 10, 'เสาไห้', 19),
(14, 11, 'มวกเหล็ก', 19),
(15, 12, 'วังม่วง', 19),
(16, 13, 'เฉลิมพระเกียรติ', 19);

INSERT INTO `region` (`regionId`, `regionCode`, `regionName`) VALUES
(1, 2, 'ภาคกลาง'),
(3, 4, 'ภาคเหนือ');

INSERT INTO `users` (`usersId`, `userName`, `prefix`, `password`, `fName`, `lName`, `status`, `trId`) VALUES
(1, 'admin', 1, 'AmVyxfpZdi9b2da0D0NSuIvXL4Hdxz75ugvoq6bXn/qjijP9KhXd6tKbROsCB7HsvGCMrNnjTNSK518jCvhbKw==', 'admin', 'admin', 1, 0),
(2, 'user', 2, 'AmVyxfpZdi9b2da0D0NSuIvXL4Hdxz75ugvoq6bXn/qjijP9KhXd6tKbROsCB7HsvGCMrNnjTNSK518jCvhbKw==', 'user', 'user', 1, 1);

INSERT INTO `prefixs` (`pId`, `pName`) VALUES
(1, 'นาย'),
(2, 'นาง'),
(3, 'นางสาว'),
(4, 'ดร.');

INSERT INTO `village` (`villageId`, `EACode`, `villageNumber`, `villageName`) VALUES
(1, '1913042004001', 1, 'พุแค'),
(2, '1913042004002', 8, 'พุแคพัฒนา'),
(3, '1913062006001', 3, 'คุ้งเขาเขียว');

INSERT INTO `activity` (`activityId`, `aName`) VALUES
(0, 'ไม่มี'),
(1, 'การผลิต (10 - 33)'),
(2, 'การจัดการและการบำบัดน้ำเสีย ของเสียและสิ่งปฏิกูล (37 - 39)'),
(3, 'การก่อสร้าง (41 - 43)'),
(4, 'การขาย - ซ่อมแซมยานยนต์และจักรยานยนต์ (45)'),
(5, 'การขายส่ง (46)'),
(6, 'การขายปลีก (47)'),
(7, 'การขนส่งทางบกและสถานที่เก็บสินค้า (49 - 53)'),
(8, 'ที่พักแรม (55)'),
(9, 'บริการอาหารและเครื่องดื่ม (56)'),
(10, 'ข้อมูลข่าวสารและการสื่อสาร (58 - 63)'),
(11, 'กิจกรรมอสังหาริมทรัพย์ (68)'),
(12, 'กิจกรรมทางวิชาชีพ วิทยาศาสตร์และเทคนิค (69 - 75)'),
(13, 'กิจกรรมการบริการและกิจกรรมสนับสนุน (77 - 82)'),
(14, 'ศิลปะ ความบันเทิงและนันทนาการ (90 - 93)'),
(15, 'กิจกรรมบริการด้านอื่นๆ (95 - 96)'),
(16, 'กิจกรรมด้านโรงพยาบาลเอกชน (86101 - 86102)');

INSERT INTO `formateconomy` (`feId`, `feName`) VALUES
(0, 'ไม่มี'),
(1, 'สำนักงานแห่งเดียว'),
(2, 'สำนักงานใหญ่'),
(3, 'สำนักงานสาขา'),
(4, 'อื่นๆ');

INSERT INTO `formatlaw` (`flId`, `flName`) VALUES
(0, 'ไม่มี'),
(1, 'ส่วนบุคคล'),
(2, 'หจก. / หสน.'),
(3, 'บจก. / บมจ.'),
(4, 'ส่วนราชการ รัฐวิสาหกิจ'),
(5, 'สหกรณ์'),
(6, 'กลุ่มแม่บ้าน'),
(7, 'สมาคม'),
(8, 'มูลนิธิ'),
(9, 'อื่น ๆ');

INSERT INTO `status` (`statusId`, `sName`) VALUES
(1, 'นับจดได้'),
(2, 'นับจดไม่ได้แต่มีข้อมูลเดิม'),
(3, 'นับจดไม่ได้แต่ไม่มีข้อมูลเดิม'),
(4, 'ย้าย / หาไม่พบ'),
(5, 'เลิกกิจการ'),
(6, 'นอกข่ายคุ้มรวม'),
(7, 'เป็นที่อยู่อาศัย'),
(8, 'อื่น ๆ');

INSERT INTO `tsic` (`tsId`, `tsic`, `isic`, `tsName`, `ActivityId`) VALUES
(0, 0, 0, 'ไม่มี', 0),
(1, 10111, NULL, 'การฆ่าสัตว์ (ยกเว้นสัตว์ปีก)', 1),
(2, 37000, NULL, 'การจัดการน้ำเสีย', 2),
(3, 41001, NULL, 'การก่อสร้างอาคารที่อยู่อาศัย', 3),
(4, 45101, NULL, 'การขายยานยนต์ใหม่ชนิดรถยนต์นั่งส่วนบุคคล รถกระบะ รถตู้ และรถขนาดเล็กที่\r\nคล้ายกัน', 4),
(5, 46101, NULL, 'การขายส่งวัตถุดิบทางการเกษตรและสัตว์มีชีวิต โดยได้รับค่าตอบแทนหรือตามสัญญาจ้าง', 5),
(6, 47111, NULL, 'ซุปเปอร์มาร์เก็ต', 6),
(7, 49201, NULL, 'การขนส่งผู้โดยสารทางรถโดยสารประจำทางในกรุงเทพฯ และปริมณฑล', 7),
(8, 55101, NULL, 'โรงแรมและรีสอร์ท', 8),
(9, 56101, NULL, 'การบริการอาหารในภัตตาคาร/ร้านอาหาร', 9),
(10, 58111, NULL, 'การจัดพิมพ์จำหน่ายหรือเผยแพร่ตำราเรียน พจนานุกรม และสารานุกรม ลงบนสื่อต่างๆ\r\n(ยกเว้นทางออนไลน์)', 10),
(11, 68101, NULL, 'การซื้อและการขายอสังหาริมทรัพย์ที่เป็นของตนเองเพื่อการอยู่อาศัย', 11),
(12, 69100, NULL, 'กิจกรรมทางกฎหมาย', 12),
(13, 77101, NULL, 'การให้เช่ายานยนต์ชนิดนั่งส่วนบุคคล รถกระบะ รถตู้ และรถขนาดเล็กที่คล้ายกัน', 13),
(14, 86101, NULL, 'กิจกรรมโรงพยาบาล (ยกเว้นโรงพยาบาลเฉพาะทาง)', 16),
(15, 90001, NULL, 'กิจกรรมการสร้างสรรค์ศิลปะ', 14),
(16, 95110, NULL, 'การซ่อมคอมพิวเตอร์และอุปกรณ์ต่อพ่วง', 15);

INSERT INTO `address` (`addressId`, `road`, `alley`, `place`, `aNum`) VALUES
(2, '-', '-', '-', '50'),
(3, '-', '-', '-', '50'),
(4, '-', '-', '-', '50'),
(7, '-', '-', '-', '50'),
(14, '-', '-', '-', '99'),
(15, '-', '-', '-', '98'),
(16, '-', '-', '-', '88/77'),
(17, '-', '-', '-', '445'),
(18, '-', '-', '-', '12/34'),
(19, '-', '-', '-', '21/161'),
(21, '-', '-', '-', '55/55'),
(22, '-', '-', '-', '69/96'),
(23, '-', '-', '-', '9/1'),
(24, '-', '-', '-', '678');

INSERT INTO `company` (`comId`, `titleName`, `fName`, `lName`, `cName`) VALUES
(2, 'นาย', 'จอร์น', 'ออฟฟิศ', ''),
(3, '-', '-', '-', '-'),
(4, 'นาย', 'จอร์น', 'เอฟ เคนนาดี้', ''),
(5, 'นาย', 'จอร์น', 'ซีนา', ''),
(6, '-', '-', '-', '-'),
(10, 'นาย', 'จอร์น', 'ชาวไร่', ''),
(11, '-', '-', '-', '-'),
(18, 'นาย', 'จอร์น', 'วิค', ''),
(19, 'นาย', 'จอห์น', ' เทอร์รี่', ''),
(20, 'นาย', 'สมชาย', 'สวัสดี', ''),
(21, 'นาง', 'สมศรี', 'สวัสดี', ''),
(22, 'นาย', 'สมหมาย', 'แซ่หลี', ''),
(23, 'หจก', '', '', 'เจริญทรัพย์พานิช'),
(25, 'นาย', 'สมปอง', 'สวัสดี', ''),
(26, 'นาง', 'สมสมัย', 'สวัสดี', ''),
(27, 'นาย', 'อำนาจ', 'สวัสดี', ''),
(28, 'นาย', 'อำนวย', 'สวัสดี', '');

INSERT INTO `hotel` (`hId`, `hType`, `hRoom`, `hMaxPrice`, `hMinPrice`) VALUES
(1, 2, 0, 0, 0),
(2, 1, 1, 200, 500);

INSERT INTO `mainbranch` (`mbId`, `road`, `alley`, `place`, `aNum`, `provinceCode`, `districtCode`, `cantonCode`, `CompanyId`) VALUES
(1, '-', '-', '-', 0, '19', '13', '04', 3),
(2, '--', '-', '-', 0, '19', '11', '09', 6),
(4, '-', '-', '--', 0, '19', '11', '05', 11);

INSERT INTO `census` (`censusId`, `dateKey`, `dateUpdate`, `yearCreate`, `regionCode`, `provinceCode`, `districtCode`, `cantonCode`, `areaCode`, `areaNumber`, `eaCode`, `censusCode`, `no`, `otherStatus`, `typeActivity`, `otherFEName`, `otherFLName`, `RegisTypeId`, `personalId`, `nationPartnerId`, `totalWorkers`, `workers`, `yearActivity`, `hospitalBed`, `useComputerId`, `useInternetId`, `saleOwnWeb`, `saleOtherWeb`, `saleEmarket`, `saleSocial`, `notSale`, `tel`, `web`, `email`, `note`, `AddressId`, `CompanyId`, `StatusId`, `ActivityId`, `FormatLawId`, `FormatEconomyId`, `HotelId`, `TSICId`, `MainBranchId`, `operatorId`, `dateOperator`, `editorialStaffId`, `dateEditorialStaff`, `recorderStaffId`, `dateRecorderStaff`, `examinerId`, `dateExaminer`) VALUES
(1, '2560-05-18', '2560-05-18', '2560', '2', '19', '13', '04', '2', '004', '001', '601913042001001', '001', '', '-', '', '', 0, '', 0, 1, 2, 1, 0, 2, 2, 0, 0, 0, 0, 1, '-', '-', '-', 'default', 7, 10, 1, 4, 4, 3, NULL, 55101, 4, 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18'),
(2, '2560-05-18', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '001', '601913042001002', '002', '', 'ผลิตไข่ต้ม', '', '', 1, '1', 0, 20, 20, 1, 0, 1, 1, 0, 0, 0, 1, 0, '-', '-', '-', 'สาขา', 2, 2, 1, 1, 1, 3, NULL, 10111, 1, 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18'),
(3, '2560-05-18', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '001', '601913042001003', '003', '', 'โฮลเท็ล', '', '', 0, '1', 0, 100, 90, 12, 0, 1, 1, 0, 1, 1, 0, 0, '-', '-', '-', 'โรงแรม', 3, 4, 1, 8, 2, 2, 1, 55101, NULL, 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18'),
(4, '2560-05-18', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '001', '601913042001004', '004', '', 'บังกะโล', '', '', 1, '1', 0, 20, 10, 2, 0, 1, 1, 0, 0, 0, 0, 1, '-', '-', '-', 'full', 4, 5, 1, 8, 1, 3, 2, 55101, 2, 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18', 'user', '2560-05-18'),
(6, '2559-05-18', '2559-05-18', '2559', '2', '19', '13', '04', '2', '004', '001', '591913042001001', '001', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 14, 18, 3, 0, 0, 0, NULL, 0, NULL, 'user', '2559-05-18', 'user', '2559-05-18', 'user', '2559-05-18', 'user', '2559-05-18'),
(7, '2550-05-18', '2550-05-18', '2550', '2', '19', '13', '04', '2', '004', '001', '501913042001001', '001', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 15, 19, 6, 0, 0, 0, NULL, 0, NULL, 'user', '2550-05-18', 'user', '2550-05-18', 'user', '2550-05-18', 'user', '2550-05-18'),
(8, '2560-05-19', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '002', '601913042002001', '001', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 16, 20, 3, 0, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(9, '2560-05-19', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '002', '601913042002002', '002', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 17, 21, 4, 0, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(10, '2560-05-19', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '002', '601913042002003', '003', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 18, 22, 7, 0, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(11, '2560-05-19', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '002', '601913042002004', '004', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 19, 23, 5, 0, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(12, '2560-05-19', '2560-05-19', '2560', '2', '19', '13', '04', '2', '004', '001', '601913042001005', '005', '', 'ขายขนมครก', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 21, 25, 2, 9, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(13, '2560-05-19', '0000-00-00', '2560', '2', '19', '13', '04', '2', '004', '001', '601913042001006', '006', '', 'ร้านนวดแผนไทย', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 22, 26, 2, 15, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(14, '2560-05-19', '0000-00-00', '2559', '2', '19', '13', '04', '2', '004', '001', '591913042001002', '002', '', 'คาราโอเกะ', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 23, 27, 2, 14, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19'),
(15, '2560-05-19', '0000-00-00', '2559', '2', '19', '13', '04', '2', '004', '001', '591913042001003', '003', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '', 24, 28, 4, 0, 0, 0, NULL, 0, NULL, 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19', 'admin', '2560-05-19');

