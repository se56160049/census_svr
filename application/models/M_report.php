<?php 
class M_report extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('census', TRUE);
	}
	
	public function GetReport1 ($year)
	{
		$lastYear = $year-1;
		
		$sql = "SELECT '".$lastYear."' AS year,
			 (SELECT COUNT(*) FROM census WHERE StatusId = 1 AND censusCode LIKE '".$lastYear."%') AS status1,
			 (SELECT COUNT(*) FROM census WHERE StatusId = 2 AND censusCode LIKE '".$lastYear."%') AS status2,
             (SELECT COUNT(*) FROM census WHERE StatusId = 3 AND censusCode LIKE '".$lastYear."%') AS status3,
             (SELECT COUNT(*) FROM census WHERE StatusId = 4 AND censusCode LIKE '".$lastYear."%') AS status4,
             (SELECT COUNT(*) FROM census WHERE StatusId = 5 AND censusCode LIKE '".$lastYear."%') AS status5,
             (SELECT COUNT(*) FROM census WHERE StatusId = 6 AND censusCode LIKE '".$lastYear."%') AS status6,
             (SELECT COUNT(*) FROM census WHERE StatusId = 7 AND censusCode LIKE '".$lastYear."%') AS status7,
             (SELECT COUNT(*) FROM census WHERE StatusId = 8 AND censusCode LIKE '".$lastYear."%') AS status8
			 UNION
			 SELECT '".$year."' AS year,
			 (SELECT COUNT(*) FROM census WHERE StatusId = 1 AND censusCode LIKE '".$year."%') AS status1,
			 (SELECT COUNT(*) FROM census WHERE StatusId = 2 AND censusCode LIKE '".$year."%') AS status2,
             (SELECT COUNT(*) FROM census WHERE StatusId = 3 AND censusCode LIKE '".$year."%') AS status3,
             (SELECT COUNT(*) FROM census WHERE StatusId = 4 AND censusCode LIKE '".$year."%') AS status4,
             (SELECT COUNT(*) FROM census WHERE StatusId = 5 AND censusCode LIKE '".$year."%') AS status5,
             (SELECT COUNT(*) FROM census WHERE StatusId = 6 AND censusCode LIKE '".$year."%') AS status6,
             (SELECT COUNT(*) FROM census WHERE StatusId = 7 AND censusCode LIKE '".$year."%') AS status7,
             (SELECT COUNT(*) FROM census WHERE StatusId = 8 AND censusCode LIKE '".$year."%') AS status8";
			 
			 
		$query = $this->db->query($sql);
		return $query;
	}

	public function getYear(){
		$sql = "SELECT  SUBSTRING(censusCode, 1, 2) AS year FROM census GROUP BY year DESC";
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function getYears(){
		$sql = "SELECT  yearCreate AS year FROM census GROUP BY year DESC";
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function getProvince(){
		$sql = "SELECT  census.provinceCode AS province ,  province.provinceName AS name FROM census INNER JOIN province ON census.provinceCode = province.provinceCode GROUP BY census.provinceCode ";
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function getDistrict($province){
		$sql = "SELECT  district.districtCode AS district ,  district.districtName AS name FROM district INNER JOIN province ON district.provinceCode = province.provinceCode WHERE province.provinceCode = ".$province;
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function getCanton($canton){
		$sql = "SELECT  canton.cantonCode AS canton ,  canton.cantonName AS name FROM canton Where canton.cantonCode like '".$canton."%'";
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetReportPie ($year)
	{
		//$lastYear = $year-1;
		
		$sql = "SELECT '".$year."' AS year,
			 (SELECT COUNT(*) FROM census WHERE StatusId = 1 AND censusCode LIKE '".$year."%') AS status1,
			 (SELECT COUNT(*) FROM census WHERE StatusId = 2 AND censusCode LIKE '".$year."%') AS status2,
             (SELECT COUNT(*) FROM census WHERE StatusId = 3 AND censusCode LIKE '".$year."%') AS status3,
             (SELECT COUNT(*) FROM census WHERE StatusId = 4 AND censusCode LIKE '".$year."%') AS status4,
             (SELECT COUNT(*) FROM census WHERE StatusId = 5 AND censusCode LIKE '".$year."%') AS status5,
             (SELECT COUNT(*) FROM census WHERE StatusId = 6 AND censusCode LIKE '".$year."%') AS status6,
             (SELECT COUNT(*) FROM census WHERE StatusId = 7 AND censusCode LIKE '".$year."%') AS status7,
             (SELECT COUNT(*) FROM census WHERE StatusId = 8 AND censusCode LIKE '".$year."%') AS status8";
			 
			 
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function GetAllStatistics($year,$province)
	{
		//$lastYear = $data-1;
		
		$sql = "SELECT activity.aName as aName, COUNT(cen1.activityId) as num1,
COALESCE(ROUND(
    				(COUNT(cen1.activityId)/(SELECT COUNT(*) as sum FROM census where yearCreate = ".$year." and provinceCode = ".$province." and activityId != 0)
                    )*100,2),0) as percen1 
FROM `activity` 
LEFT JOIN (SELECT * FROM census WHERE yearCreate = ".$year." and provinceCode = ".$province." and activityId != 0) as cen1 ON activity.activityId = cen1.ActivityId
where activity.activityId !=0
GROUP BY activity.aName ORDER BY activity.activityId ASC";
			 	 
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetAllStatisticsDistrict($year,$province,$district)
	{
		//$lastYear = $data-1;
		
		$sql = "SELECT activity.aName as aName, COUNT(cen1.activityId) as num1,
COALESCE(ROUND(
    				(COUNT(cen1.activityId)/(SELECT COUNT(*) as sum FROM census where yearCreate = ".$year." and provinceCode = ".$province." and districtCode = ".$district." and activityId != 0)
                    )*100,2),0) as percen1 
FROM `activity` 
LEFT JOIN (SELECT * FROM census WHERE yearCreate = ".$year." and provinceCode = ".$province." and districtCode = ".$district." and activityId != 0) as cen1 ON activity.activityId = cen1.ActivityId
where activity.activityId !=0
GROUP BY activity.aName ORDER BY activity.activityId ASC";
			 
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetAllStatisticsDistrictCanton($year,$province,$district,$canton)
	{
		//$lastYear = $data-1;
		
		$sql = "SELECT activity.aName as aName, COUNT(cen1.activityId) as num1,
COALESCE(ROUND(
    				(COUNT(cen1.activityId)/(SELECT COUNT(*) as sum FROM census where yearCreate = ".$year." and provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$canton." and activityId != 0)
                    )*100,2),0) as percen1 
FROM `activity` 
LEFT JOIN (SELECT * FROM census WHERE yearCreate = ".$year." and provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$canton." and activityId != 0) as cen1 ON activity.activityId = cen1.ActivityId
where activity.activityId !=0
GROUP BY activity.aName ORDER BY activity.activityId ASC";
			 
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetAllStatus($year,$province)
	{
		$sql = "SELECT status.sName as sName, COUNT(cen1.statusId) as num1,
COALESCE(ROUND(
		(COUNT(cen1.statusId)/(SELECT COUNT(*) as sum FROM census where yearCreate =".$year." and provinceCode = ".$province.")
		)*100,2),0) as percen1 
FROM `status` 
LEFT JOIN (SELECT * FROM census WHERE yearCreate = ".$year." and provinceCode = ".$province.") as cen1 ON status.statusId = cen1.StatusId
GROUP BY status.sName ORDER BY status.statusId ASC ";
			 
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetAllStatusDistrict($year, $province,$district)
	{
		
		$sql = "SELECT status.sName as sName, COUNT(cen1.statusId) as num1,
COALESCE(ROUND(
		(COUNT(cen1.statusId)/(SELECT COUNT(*) as sum FROM census where yearCreate =".$year." and provinceCode = ".$province." and districtCode = ".$district.")
		)*100,2),0) as percen1 
FROM `status` 
LEFT JOIN (SELECT * FROM census WHERE yearCreate = ".$year." and provinceCode = ".$province." and districtCode = ".$district.") as cen1 ON status.statusId = cen1.StatusId
GROUP BY status.sName ORDER BY status.statusId ASC ";
			 
			 
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetAllStatusDistrictCanton($year, $province,$district,$canton)
	{
		
		$sql = "SELECT status.sName as sName, COUNT(cen1.statusId) as num1,
COALESCE(ROUND(
		(COUNT(cen1.statusId)/(SELECT COUNT(*) as sum FROM census where yearCreate =".$year." and provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$canton.")
		)*100,2),0) as percen1 
FROM `status` 
LEFT JOIN (SELECT * FROM census WHERE yearCreate = ".$year." and provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$canton.") as cen1 ON status.statusId = cen1.StatusId
GROUP BY status.sName ORDER BY status.statusId ASC ";
			 
			 
		$query = $this->db->query($sql);
		return $query;
	}

public function getCountStatus($year,$yearTo,$province){
		$sql = "SELECT ( SELECT COUNT(StatusId) FROM `census` WHERE yearCreate = ".$year." AND provinceCode = ".$province.") as countStatus,
					   ( SELECT COUNT(StatusId) FROM `census` WHERE yearCreate = ".$yearTo." AND provinceCode = ".$province.") as countStatuslast,
					   (SELECT provinceName FROM province WHERE provinceCode = ".$province.") as pName";
		
		$query = $this->db->query($sql);
		return $query;
	}

public function getCountStatusD($year,$yearTo,$province,$district){
		$sql = "SELECT ( SELECT COUNT(StatusId) FROM `census` WHERE yearCreate = ".$year." AND provinceCode = ".$province." and districtCode = ".$district.") as countStatus,
					   ( SELECT COUNT(StatusId) FROM `census` WHERE yearCreate = ".$yearTo." AND provinceCode = ".$province." and districtCode = ".$district.") as countStatuslast,
                         (SELECT provinceName FROM province WHERE provinceCode = ".$province.") AS pName,
                        (SELECT district.districtName FROM district INNER JOIN province ON district.provinceCode = province.provinceCode WHERE district.districtCode = ".$district." and province.provinceCode = ".$province." ) AS dName ";
		
		$query = $this->db->query($sql);
		return $query;
	}

public function getCountStatusC($year,$yearTo,$province,$district,$cantons,$canton){
		$sql = "SELECT ( SELECT COUNT(StatusId) FROM `census` WHERE yearCreate = ".$year." AND provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$cantons.") as countStatus,
					   ( SELECT COUNT(StatusId) FROM `census` WHERE yearCreate = ".$yearTo." AND provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$cantons.") as countStatuslast,
					    (SELECT provinceName FROM province WHERE provinceCode = ".$province.") AS pName,
                        (SELECT district.districtName FROM district INNER JOIN province ON district.provinceCode = province.provinceCode WHERE district.districtCode = ".$district." and province.provinceCode = ".$province." ) AS dName,
                        (SELECT cantonName FROM canton WHERE cantonCode = ".$canton." ) AS cName";
		
		$query = $this->db->query($sql);
		return $query;
	}

public function getCountStatistics($year,$yearTo,$province){
		$sql = "SELECT ( SELECT COUNT(ActivityId) FROM `census` WHERE yearCreate = ".$year." AND provinceCode = ".$province." and activityId != 0) as countStatus,
					   ( SELECT COUNT(ActivityId) FROM `census` WHERE yearCreate = ".$yearTo." AND provinceCode = ".$province." and activityId != 0) as countStatuslast,
					    (SELECT provinceName FROM province WHERE provinceCode = ".$province.") as pName";
		
		$query = $this->db->query($sql);
		return $query;
	}

public function getCountStatisticsD($year,$yearTo,$province,$district){
		$sql = "SELECT ( SELECT COUNT(ActivityId) FROM `census` WHERE yearCreate = ".$year." AND provinceCode = ".$province." and districtCode = ".$district." and activityId != 0) as countStatus,
					   ( SELECT COUNT(ActivityId) FROM `census` WHERE yearCreate = ".$yearTo." AND provinceCode = ".$province." and districtCode = ".$district." and activityId != 0) as countStatuslast,
					   (SELECT provinceName FROM province WHERE provinceCode = ".$province.") AS pName,
                        (SELECT district.districtName FROM district INNER JOIN province ON district.provinceCode = province.provinceCode WHERE district.districtCode = ".$district." and province.provinceCode = ".$province." ) AS dName ";
		
		$query = $this->db->query($sql);
		return $query;
	}

public function getCountStatisticsC($year,$yearTo,$province,$district,$cantons,$canton){
		$sql = "SELECT ( SELECT COUNT(ActivityId) FROM `census` WHERE yearCreate = ".$year." AND provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$cantons." and activityId != 0) as countStatus,
					   ( SELECT COUNT(ActivityId) FROM `census` WHERE yearCreate = ".$yearTo." AND provinceCode = ".$province." and districtCode = ".$district." and cantonCode = ".$cantons." and activityId != 0) as countStatuslast,
					   (SELECT provinceName FROM province WHERE provinceCode = ".$province.") AS pName,
                        (SELECT district.districtName FROM district INNER JOIN province ON district.provinceCode = province.provinceCode WHERE district.districtCode = ".$district." and province.provinceCode = ".$province." ) AS dName,
                        (SELECT cantonName FROM canton WHERE cantonCode = ".$canton." ) AS cName";
		
		$query = $this->db->query($sql);
		return $query;
	}

}