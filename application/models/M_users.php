<?php 
class M_users extends CI_Model
{

	
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('census', TRUE);
	}
	
	public function test_users ()
	{
		$sql = "SELECT usersId, userName, prefix, prefixs.pName as pName, prefix, password, fName, lName, status, trId FROM users INNER JOIN prefixs ON users.prefix = prefixs.pId ORDER BY status ASC ";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function test_search ($id)
	{
		//$sql = 'SELECT usersId, userName, prefix, password, fName, lName, status FROM users where fName like "%'. $id . '%" ';
		$sql = "SELECT  census.censusId,census.dateKey AS dateKey, census.censusCode AS censusCode, census.typeActivity AS typeActivity, company.cName AS comName, company.fName AS fristName, company.lName AS lastName, company.titleName AS titleName, 
						status.sName AS statusName, status.statusId AS statusId, area.areaName AS aName, canton.cantonName AS cName,district.districtName AS dName, province.provinceName AS pName, region.regionName AS rName, activity.activityId AS actName,
						address.road AS road, address.alley AS alley, address.place AS place, address.aNum AS aNum, census.dateupdate AS dateupdate , tsic.tsName AS tsName, tsic.tsic AS tsic FROM census 
				LEFT JOIN region ON census.regionCode = region.regionCode
                LEFT JOIN province ON census.provinceCode = province.provinceCode
                LEFT JOIN district ON census.districtCode = district.districtCode
                LEFT JOIN canton ON census.cantonCode = canton.cantonCode
                LEFT JOIN area ON census.areaNumber = area.areaNumber
                LEFT JOIN company ON census.CompanyId = company.comId
				LEFT JOIN activity ON census.ActivityId = activity.activityId
				LEFT JOIN status ON census.StatusId = status.statusId
				LEFT JOIN address ON census.AddressId = address.addressId
				LEFT JOIN tsic ON census.TSICId = tsic.tsic
				WHERE  ( 
						
						census.yearCreate LIKE '%{$id}%'  OR
						census.censusCode LIKE '%{$id}%'  OR
						census.typeActivity LIKE '%{$id}%'  OR
						company.cName LIKE '%{$id}%'  OR
						company.fName LIKE '%{$id}%'  OR
						company.lName LIKE '%{$id}%'  OR
						canton.cantonName LIKE '%{$id}%'   OR
						district.districtName LIKE '%{$id}%'   OR 
						province.provinceName LIKE '%{$id}%'  OR 
						region.regionName LIKE '%{$id}%'  OR 
						activity.aName LIKE '%{$id}%'  OR 
						area.areaName LIKE '%{$id}%' OR
						tsic.tsic LIKE '%{$id}%' OR
						tsic.tsName LIKE '%{$id}%'
						) ORDER BY census.censusCode ASC";
		// var_dump($id);
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function m_check_search ($id)
	{
		//$sql = 'SELECT usersId, userName, prefix, password, fName, lName, status FROM users where fName like "%'. $id . '%" ';
		$sql = "SELECT  census.censusId,census.dateKey AS dateKey, census.censusCode AS censusCode, census.typeActivity AS typeActivity, company.cName AS comName, company.fName AS fristName, company.lName AS lastName, company.titleName AS titleName, 
						status.sName AS statusName, status.statusId AS statusId, area.areaName AS aName, canton.cantonName AS cName,district.districtName AS dName, province.provinceName AS pName, region.regionName AS rName, activity.activityId AS actName,
						address.road AS road, address.alley AS alley, address.place AS place, address.aNum AS aNum , census.dateupdate AS dateupdate, tsic.tsName AS tsName, tsic.tsic AS tsic FROM census 
				LEFT JOIN region ON census.regionCode = region.regionCode
                LEFT JOIN province ON census.provinceCode = province.provinceCode
                LEFT JOIN district ON census.districtCode = district.districtCode
                LEFT JOIN canton ON census.cantonCode = canton.cantonCode
                LEFT JOIN area ON census.areaNumber = area.areaNumber
                LEFT JOIN company ON census.CompanyId = company.comId
				LEFT JOIN activity ON census.ActivityId = activity.activityId
				LEFT JOIN status ON census.StatusId = status.statusId
				LEFT JOIN address ON census.AddressId = address.addressId
				LEFT JOIN tsic ON census.TSICId = tsic.tsic
				WHERE  ( 
						census.dateKey LIKE '%{$id}%'  OR
						census.yearCreate LIKE '%{$id}%'  OR
						census.censusCode LIKE '%{$id}%'  OR
						census.typeActivity LIKE '%{$id}%'  OR
						company.cName LIKE '%{$id}%'  OR
						company.fName LIKE '%{$id}%'  OR
						company.lName LIKE '%{$id}%'  OR
						canton.cantonName LIKE '%{$id}%'   OR
						district.districtName LIKE '%{$id}%'   OR 
						province.provinceName LIKE '%{$id}%'  OR 
						region.regionName LIKE '%{$id}%'  OR 
						activity.aName LIKE '%{$id}%'  OR 
						area.areaName LIKE '%{$id}%' OR
						tsic.tsic LIKE '%{$id}%' OR
						tsic.tsName LIKE '%{$id}%'
						) ORDER BY census.censusCode ASC ";
	
		$query = $this->db->query($sql);
		$num =  $query->num_rows();
		return $num;
	}
	
	public function user_del($id){
		$sql = 'UPDATE users SET status = 2 WHERE usersId = '.$id;
        $this->db->query($sql);
	}

	public function users_show ($id)
	{
		$sql = 'SELECT usersId, userName, prefixs.pName as pName, prefix, password, fName, lName, status, trId FROM users INNER JOIN prefixs ON users.prefix = prefixs.pId where users.usersId = '.$id;
		$query = $this->db->query($sql);
		return $query;
	}

	public function insert_user($data)
    {
		$error;
        $sql = 'INSERT INTO users (userName,prefix,password,fName,lName,status,trId) VALUE (?,?,?,?,?,?,?)';
		
		if(!$this->db->query($sql, array($data['username'],$data['prefix'],$data['password'],$data['fname'], $data['lname'],1, $data['trId']))){
			$error = $this->db->error();
		}else {
			$this->db->query($sql, array($data['username'],$data['prefix'],$data['password'],$data['fname'], $data['lname'],1, $data['trId']));
			$error = 'OK';
		}
		return $error;		
    }

	public function update_user($data)
	{
		$sql = 'UPDATE users SET userName = ?, prefix = ?, password = ?, fName = ?, lName = ? , status = ? , trId = ? where usersId =  ?';
        
			if(!$this->db->query($sql, array($data['username'], $data['prefix'], $data['password'], $data['fname'] ,$data['lname'],$data['status'],$data['trId'],$data['userid']))){
			$error = $this->db->error();
		}else {
			$this->db->query($sql, array($data['username'], $data['prefix'], $data['password'], $data['fname'] ,$data['lname'],$data['status'],$data['trId'],$data['userid']));
			$error = 'OK';
		}
		return $error;		
	}

	public function type_user ()
	{
		$sql = "SELECT trId, trName from typerecorder";
		$query = $this->db->query($sql);
		return $query;
	}

	public function m_check_search_ADBV($data,$regionId)
	{
		//$sqlWhereNumber = '';
		//$sqlWhereName = '';
		//$sqlWhereStatus = '';
		//if($number != ''){
		//	$sqlWhereNumber = " and census.censusCode LIKE '{$number}%'";
	//	}
		//if($name != ''){
		//	//$sqlWhereName = "and census.censusCode LIKE '{$number}%'";
		//}
		//if($status > 0){
			//$sqlWhereStatus = " and census.statusId = {$status}";
		//}
		
		//$sql = 'SELECT usersId, userName, prefix, password, fName, lName, status FROM users where fName like "%'. $id . '%" ';
		$sql = "SELECT  census.censusId,census.dateKey AS dateKey, census.censusCode AS censusCode, census.typeActivity AS typeActivity, company.cName AS comName, company.fName AS fristName, company.lName AS lastName, company.titleName AS titleName, 
						status.sName AS statusName, status.statusId AS statusId, area.areaName AS aName, canton.cantonName AS cName,district.districtName AS dName, province.provinceName AS pName, region.regionName AS rName, activity.activityId AS actName,
						address.road AS road, address.alley AS alley, address.place AS place, address.aNum AS aNum , census.dateupdate AS dateupdate, tsic.tsName AS tsName, tsic.tsic AS tsic FROM census 
				LEFT JOIN region ON census.regionCode = region.regionCode
                LEFT JOIN province ON census.provinceCode = province.provinceCode
                LEFT JOIN district ON census.districtCode = district.districtCode
                LEFT JOIN canton ON census.cantonCode = canton.cantonCode
                LEFT JOIN area ON census.areaNumber = area.areaNumber
                LEFT JOIN company ON census.CompanyId = company.comId
				LEFT JOIN activity ON census.ActivityId = activity.activityId
				LEFT JOIN status ON census.StatusId = status.statusId
				LEFT JOIN address ON census.AddressId = address.addressId
				LEFT JOIN tsic ON census.TSICId = tsic.tsic
				WHERE census.censusCode LIKE '{$data}%' and census.regionCode = ".$regionId;
		// var_dump($id);
		
		$query = $this->db->query($sql);
		$num =  $query->num_rows();
		return $num;
	}

	public function m_search_ADBV($data,$regionId,$number,$name,$status)
	{
		$sqlWhereNumber = '';
		$sqlWhereName = '';
		$sqlWhereStatus = '';
		if($number != ''){
			$sqlWhereNumber = " and census.censusCode LIKE '{$number}%'";
		}
		if($name != ''){
			$sqlWhereName = " and (company.cName LIKE '%{$name}%'  OR
						company.fName LIKE '%{$name}%'  OR
						company.lName LIKE '%{$name}%'  )";

		}
		if($status > 0){
			$sqlWhereStatus = " and census.statusId = {$status}";
		}
		//$sql = 'SELECT usersId, userName, prefix, password, fName, lName, status FROM users where fName like "%'. $id . '%" ';
		$sql = "SELECT  census.censusId,census.dateKey AS dateKey, census.censusCode AS censusCode, census.typeActivity AS typeActivity, company.cName AS comName, company.fName AS fristName, company.lName AS lastName, company.titleName AS titleName, 
						status.sName AS statusName, status.statusId AS statusId, area.areaName AS aName, canton.cantonName AS cName,district.districtName AS dName, province.provinceName AS pName, region.regionName AS rName, activity.activityId AS actName,
						address.road AS road, address.alley AS alley, address.place AS place, address.aNum AS aNum , census.dateupdate AS dateupdate, tsic.tsName AS tsName, tsic.tsic AS tsic FROM census 
				LEFT JOIN region ON census.regionCode = region.regionCode
                LEFT JOIN province ON census.provinceCode = province.provinceCode
                LEFT JOIN district ON census.districtCode = district.districtCode
                LEFT JOIN canton ON census.cantonCode = canton.cantonCode
                LEFT JOIN area ON census.areaNumber = area.areaNumber
                LEFT JOIN company ON census.CompanyId = company.comId
				LEFT JOIN activity ON census.ActivityId = activity.activityId
				LEFT JOIN status ON census.StatusId = status.statusId
				LEFT JOIN address ON census.AddressId = address.addressId
				LEFT JOIN tsic ON census.TSICId = tsic.tsic
				WHERE census.censusCode LIKE '{$data}%' and census.regionCode = ".$regionId.$sqlWhereNumber.$sqlWhereStatus.$sqlWhereName;
		// var_dump($id);
		//echo $sql;
		
		$query = $this->db->query($sql);
		return $query;
	}

	public function check_login ($us){
	
		$sql = "SELECT * FROM `users` WHERE userName = '".$us."' and status = 1";
		$query = $this->db->query($sql);
		return $query;
	}

	public function getResidueInfo($year){
		$sql = "SELECT census.censusId,census.dateKey AS dateKey, census.censusCode AS censusCode, census.typeActivity AS typeActivity, company.cName AS comName, company.fName AS fristName, company.lName AS lastName, company.titleName AS titleName, 
						status.sName AS statusName, status.statusId AS statusId, area.areaName AS aName, canton.cantonName AS cName,district.districtName AS dName, province.provinceName AS pName, region.regionName AS rName, activity.activityId AS actName,
						address.road AS road, address.alley AS alley, address.place AS place, address.aNum AS aNum , census.dateupdate AS dateupdate, tsic.tsName AS tsName, tsic.tsic AS tsic FROM census 
				LEFT JOIN region ON census.regionCode = region.regionCode
                LEFT JOIN province ON census.provinceCode = province.provinceCode
                LEFT JOIN district ON census.districtCode = district.districtCode
                LEFT JOIN canton ON census.cantonCode = canton.cantonCode
                LEFT JOIN area ON census.areaNumber = area.areaNumber
                LEFT JOIN company ON census.CompanyId = company.comId
				LEFT JOIN activity ON census.ActivityId = activity.activityId
				LEFT JOIN status ON census.StatusId = status.statusId
				LEFT JOIN address ON census.AddressId = address.addressId
				LEFT JOIN tsic ON census.TSICId = tsic.tsic
                WHERE yearCreate < {$year} && dateUpdate < '{$year}-01-01'";

		
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function getPrefix(){
		$sql = "SELECT * FROM prefixs";
		$query = $this->db->query($sql);
		return $query;
	}
	public function insert_prefix($name){
			$sql = "INSERT INTO prefixs (pName) VALUES ('{$name}')";
			$this->db->query($sql);
			return $this->db->insert_id();
	}

}