<?php 
class M_inputs extends CI_Model {

	
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('census', TRUE);
	}

	public function getStatus() {
        $sql = "SELECT * FROM status";
		$query = $this->db->query($sql);
		return $query;
    }

	public function getActivity() {
        $sql = "SELECT * FROM activity WHERE activityID != 0";
		$query = $this->db->query($sql);
		return $query;
    }

	public function getFormatLaw() {
        $sql = "SELECT flId, flName FROM formatlaw WHERE flId != 0";
		$query = $this->db->query($sql);
		return $query;
    }

	public function getFormatEconomy() {
        $sql = "SELECT feId, feName FROM formateconomy WHERE feId != 0";
		$query = $this->db->query($sql);
		return $query;
    }

	public function getUsers() {
        $sql = "SELECT userName, fName, lName FROM users WHERE status = 1 and userName NOT LIKE '%admin%'";
		$query = $this->db->query($sql);
		return $query;
    }

	public function getTSIC($acId) {
        $sql = "SELECT tsic, tsName FROM tsic WHERE ActivityId = {$acId} and tsId != 0";
		$query = $this->db->query($sql);
		return $query;
    }

	public function getFormCensusId($id) {
		$sql = "SELECT c.yearCreate, c.regionCode, c.provinceCode, c.districtCode, c.cantonCode, c.areaCode, c.areaNumber, c.eaCode, a.road,  a.alley, a.place, a.aNum, co.titleName, co.fName, co.lName, co.cName,
		StatusId, otherStatus, ActivityId, typeActivity, FormatLawId, otherFLName, FormatEconomyId, otherFEName, RegisTypeId, personalId, nationPartnerId, totalWorkers, workers, yearActivity,
		hospitalBed , h.hType, h.hRoom, h.hMinPrice, h.hMaxPrice, useComputerId, useInternetId, saleOwnWeb, saleOtherWeb, saleEmarket, saleSocial, notSale, tel, web, email, TSICId, mbc.titleName AS mbTitleName,
		mbc.fName AS mbFName, mbc.lName AS mbLName, mbc.cName AS mbCName, mb.road AS mbRoad, mb.alley AS mbAlley, mb.place AS mbPlace, mb.aNum AS mbANum, mb.provinceCode AS mbProvinceId, mb.districtCode AS mbDistrictId,
		mb.cantonCode AS mbCantonId, note, c.operatorId, c.editorialStaffId, c.recorderStaffId, c.examinerId
		FROM `census` as c 
		LEFT JOIN address a ON c.`AddressId`= a.addressId
		LEFT JOIN company co ON c.CompanyId = co.comId
		LEFT JOIN hotel h ON c.HotelId = h.hId
		LEFT JOIN mainbranch mb ON c.MainBranchId = mb.mbId
		LEFT JOIN company mbc ON mb.CompanyId = mbc.comId
		WHERE `censusCode` LIKE '%{$id}%' ORDER BY c.censusCode ASC LIMIT 1";
		$query = $this->db->query($sql);
		return $query;
	}

	public function checkCensusId($censusId13Digits){
		$sql = "SELECT MAX(no) as No FROM census WHERE censusCode LIKE '{$censusId13Digits}%'";
		$query = $this->db->query($sql);
		return $query;
	}

	// Insert 4 options.
	public function insertFullInputs($data){

		$this->db->trans_begin();
		$this->fullOption($data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function insertHotelInputs($data) {
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';
		(!isset($tempHotelId)) and $tempHotelId = '';

		$this->db->trans_begin();
		$this->hotelOption($data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function insertMainBranchInputs($data){

		$this->db->trans_begin();
		$this->mbOption($data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function insertDefaultInputs($data){

		$this->db->trans_begin();
		$this->defaultOption($data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function getOptionData($data){
		$sql = "SELECT censusId, dateKey, AddressId, census.CompanyId ,HotelId, MainBranchId, mainbranch.CompanyId as mbComId FROM census 
				LEFT JOIN mainbranch ON MainBranchId = mbId 
				WHERE censusCode ='{$data}'";
		$query = $this->db->query($sql);
		return $query;
	}

	// Update 4 main Options.
	public function updateFromFullInputs($data, $addrId, $comId, $hotelId, $mbComId, $mbId){

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);
		// delete specific.
		$sql = "DELETE FROM mainbranch WHERE mbId = '{$mbId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM company WHERE comId ='{$mbComId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM hotel WHERE hId = '{$hotelId}'";
		$query = $this->db->query($sql);

		// insert.
		if ($data['ecoActivity'] == 8 && $data['formatEconomy'] == 3){
			$this->insertFullInputs($data);
		} else if ($data['ecoActivity'] == 8 && $data['formatEconomy'] != 3) {
			$this->insertHotelInputs($data);
		} else if ($data['ecoActivity'] != 8 && $data['formatEconomy'] == 3) {
			$this->insertMainBranchInputs($data);
		} else {
			$this->insertDefaultInputs($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function updateFromge2FullInputs($data, $addrId, $comId, $hotelId, $mbComId, $mbId){

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);
		// delete specific.
		$sql = "DELETE FROM mainbranch WHERE mbId = '{$mbId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM company WHERE comId ='{$mbComId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM hotel WHERE hId = '{$hotelId}'";
		$query = $this->db->query($sql);

		// insert.
		if ($data['status'] === 2){
			$this->eq2Option($data);
		} else {
			$this->ge2Option($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function updateFromHotelOption($data, $addrId, $comId, $hotelId) {

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);
		// delete specific.
		$sql = "DELETE FROM hotel WHERE hId = '{$hotelId}'";
		$query = $this->db->query($sql);

		// insert.
		if ($data['ecoActivity'] == 8 && $data['formatEconomy'] == 3){
			$this->insertFullInputs($data);
		} else if ($data['ecoActivity'] == 8 && $data['formatEconomy'] != 3) {
			$this->insertHotelInputs($data);
		} else if ($data['ecoActivity'] != 8 && $data['formatEconomy'] == 3) {
			$this->insertMainBranchInputs($data);
		} else {
			$this->insertDefaultInputs($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function updateFromge2HotelOption($data, $addrId, $comId, $hotelId) {

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);
		// delete specific.
		$sql = "DELETE FROM hotel WHERE hId = '{$hotelId}'";
		$query = $this->db->query($sql);

		if ($data['status'] === 2){
			$this->eq2Option($data);
		} else {
			$this->ge2Option($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		return $error;
	}

	public function updateFromMBInputs($data, $addrId, $comId, $mbComId, $mbId){

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);
		// delete specific.
		$sql = "DELETE FROM mainbranch WHERE mbId = '{$mbId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM company WHERE comId ='{$mbComId}'";
		$query = $this->db->query($sql);

		// insert.
		if ($data['ecoActivity'] == 8 && $data['formatEconomy'] == 3){
			$this->insertFullInputs($data);
		} else if ($data['ecoActivity'] == 8 && $data['formatEconomy'] != 3) {
			$this->insertHotelInputs($data);
		} else if ($data['ecoActivity'] != 8 && $data['formatEconomy'] == 3) {
			$this->insertMainBranchInputs($data);
		} else {
			$this->insertDefaultInputs($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}
	
	public function updateFromge2MBInputs($data, $addrId, $comId, $mbComId, $mbId){
		
		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);
		// delete specific.
		$sql = "DELETE FROM mainbranch WHERE mbId = '{$mbId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM company WHERE comId ='{$mbComId}'";
		$query = $this->db->query($sql);

		if ($data['status'] === 2){
			$this->eq2Option($data);
		} else {
			$this->ge2Option($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function updateFromDefaultInputs($data, $addrId, $comId){

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);

		// insert.
		if ($data['ecoActivity'] == 8 && $data['formatEconomy'] == 3){
			$this->insertFullInputs($data);
		} else if ($data['ecoActivity'] == 8 && $data['formatEconomy'] != 3) {
			$this->insertHotelInputs($data);
		} else if ($data['ecoActivity'] != 8 && $data['formatEconomy'] == 3) {
			$this->insertMainBranchInputs($data);
		} else {
			$this->insertDefaultInputs($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function updateFromge2DefaultInputs($data, $addrId, $comId){

		(!isset($sql)) and $sql = '';

		$this->db->trans_begin();
		// delete primary.
		$sql = "DELETE FROM `census` WHERE censusCode = '{$data['censusCode']}'";
		$query = $this->db->query($sql);
		$this->primaryDelete($addrId, $comId);

		// insert.
		if ($data['status'] === 2){
			$this->eq2Option($data);
		} else {
			$this->ge2Option($data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function insertStatuseq2Inputs($data) {
		
		$this->db->trans_begin();
		$this->eq2Option($data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	public function insertStatusge2Inputs($data) {
		$this->db->trans_begin();
		$this->ge2Option($data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$error = $this->db->error();
		}else{
			$this->db->trans_commit();
			$error = 'ok';
		}
		
		return $error;
	}

	private function eq2Option($data){
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';
		// 1. Insert Address table.
		$sql = "INSERT INTO address(road, alley, place, aNum) VALUES ('{$data['road']}', '{$data['alley']}', '{$data['place']}', '{$data['aNum']}')";
		$query = $this->db->query($sql);
		$tempAdrId = $this->db->insert_id();

		// 2. Insert Company table.
		$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['tName']}', '{$data['fName']}', '{$data['lName']}', '{$data['facName']}')";
		$query = $this->db->query($sql);
		$tempCompanyId = $this->db->insert_id();

			// 6. Insert census table.
		$sql =	"INSERT INTO census(censusId, dateKey, dateUpdate, yearCreate, regionCode, provinceCode, districtCode, cantonCode, areaCode, 
				areaNumber, eaCode, censusCode, no, typeActivity, AddressId, CompanyId, StatusId, ActivityId, operatorId, dateOperator,
				editorialStaffId, dateEditorialStaff, recorderStaffId, dateRecorderStaff, examinerId, dateExaminer) VALUES
				('{$data['censusId']}' , '{$data['dateKey']}' ,'{$data['dateUpdate']}' , '{$data['fullYear']}', '{$data['regionId']}', '{$data['provinceId']}', '{$data['districtId']}',
				'{$data['cantonId']}', '{$data['areaType']}', '{$data['areaId']}', '{$data['eaId']}', '{$data['censusCode']}',
				'{$data['no']}', '{$data['typeActivity']}', {$tempAdrId}, {$tempCompanyId}, {$data['status']}, 
				{$data['ecoActivity']}, '{$data['operatorId']}', '{$data['dateKey']}', '{$data['editorialStaffId']}', 
				'{$data['dateKey']}', '{$data['recorderStaffId']}', '{$data['dateKey']}', '{$data['examinerId']}', '{$data['dateKey']}'
				)";
		$query = $this->db->query($sql);
	}

	private function ge2Option($data) {
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';

		// 1. Insert Address table.
		$sql = "INSERT INTO address(road, alley, place, aNum) VALUES ('{$data['road']}', '{$data['alley']}', '{$data['place']}', '{$data['aNum']}')";
		$query = $this->db->query($sql);
		$tempAdrId = $this->db->insert_id();

		// 2. Insert Company table.
		$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['tName']}', '{$data['fName']}', '{$data['lName']}', '{$data['facName']}')";
		$query = $this->db->query($sql);
		$tempCompanyId = $this->db->insert_id();

			// 6. Insert census table.
		$sql =	"INSERT INTO census(censusId, dateKey, dateUpdate, yearCreate, regionCode, provinceCode, districtCode, cantonCode, areaCode, 
				areaNumber, eaCode, censusCode, no, otherStatus, AddressId, CompanyId, StatusId, operatorId, dateOperator,
				editorialStaffId, dateEditorialStaff, recorderStaffId, dateRecorderStaff, examinerId, dateExaminer) VALUES
				('{$data['censusId']}' , '{$data['dateKey']}' ,'{$data['dateUpdate']}' , '{$data['fullYear']}', '{$data['regionId']}',
				'{$data['provinceId']}', '{$data['districtId']}', '{$data['cantonId']}', '{$data['areaType']}', '{$data['areaId']}',
				'{$data['eaId']}', '{$data['censusCode']}', '{$data['no']}', '{$data['otherStatus']}', {$tempAdrId}, {$tempCompanyId}, {$data['status']},
				'{$data['operatorId']}', '{$data['dateKey']}', '{$data['editorialStaffId']}', '{$data['dateKey']}',
				'{$data['recorderStaffId']}', '{$data['dateKey']}', '{$data['examinerId']}', '{$data['dateKey']}')";
		$query = $this->db->query($sql);
	}
	private function fullOption($data){
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';
		(!isset($tempHotelId)) and $tempHotelId = '';
		(!isset($tempMBAdrId)) and $tempMBAdrId = '';
		(!isset($tempMBId)) and $tempMBId = '';

		// 1. Insert Address table.
			$sql = "INSERT INTO address(road, alley, place, aNum) VALUES ('{$data['road']}', '{$data['alley']}', '{$data['place']}', '{$data['aNum']}')";
			$query = $this->db->query($sql);
			$tempAdrId = $this->db->insert_id();

			// 2. Insert Company table.
			$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['tName']}', '{$data['fName']}', '{$data['lName']}', '{$data['facName']}')";
			$query = $this->db->query($sql);
			$tempCompanyId = $this->db->insert_id();

			// 3. Insert Hotel table.
			$sql = "INSERT INTO hotel(hType, hRoom, hMaxPrice, hMinPrice) VALUES ({$data['hType']}, {$data['hRoom']}, {$data['hMaxPrice']}, {$data['hMinPrice']})";
			$query = $this->db->query($sql);
			$tempHotelId = $this->db->insert_id();

			// 4. Insert (1)Mainbranch table.
			$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['mbTitleName']}', '{$data['mbFName']}', '{$data['mbLName']}', '{$data['mbDeptName']}')";
			$query = $this->db->query($sql);
			$tempMBAdrId = $this->db->insert_id();

			// 5. Insert (2)Mainbranch table.
			$sql = "INSERT INTO mainbranch(road, alley, place, aNum, provinceCode, districtCode, cantonCode, CompanyId) VALUES 
					('{$data['mbRoad']}', '{$data['mbAlley']}', '{$data['mbPlace']}', '{$data['mbANum']}',
					'{$data['mbProvince']}', '{$data['mbDistrict']}', '{$data['mbCanton']}', {$tempMBAdrId})";
			$query = $this->db->query($sql);
			$tempMBId = $this->db->insert_id();

			// 6. Insert census table.
			$sql =	"INSERT INTO census(censusId, dateKey, dateUpdate, yearCreate, regionCode, provinceCode, districtCode, cantonCode, areaCode, 
					areaNumber, eaCode, censusCode, no, otherStatus, typeActivity, otherFEName, otherFLName, RegisTypeId, personalId, 
					nationPartnerId, totalWorkers, workers, yearActivity, hospitalBed, useComputerId, useInternetId, saleOwnWeb, saleOtherWeb,
					saleEmarket, saleSocial, notSale, tel, web, email, note, AddressId, CompanyId, StatusId, ActivityId, FormatLawId,
					FormatEconomyId, HotelId, TSICId, MainBranchId, operatorId, dateOperator, editorialStaffId, dateEditorialStaff,
					recorderStaffId, dateRecorderStaff, examinerId, dateExaminer) VALUES
		 			('{$data['censusId']}', '{$data['dateKey']}', '{$data['dateUpdate']}', '{$data['fullYear']}', '{$data['regionId']}', '{$data['provinceId']}', '{$data['districtId']}',
		 			'{$data['cantonId']}', '{$data['areaType']}', '{$data['areaId']}', '{$data['eaId']}', '{$data['censusCode']}',
					'{$data['no']}', '{$data['otherStatus']}', '{$data['typeActivity']}', '{$data['otherFormatEconomy']}',
					'{$data['otherFormatLaw']}', {$data['regisType']}, '{$data['personalId']}', {$data['nationPartner']}, 
					{$data['totalWorkers']}, {$data['workers']}, {$data['yearActivity']}, {$data['hospitalBed']},
					{$data['useComputer']}, {$data['useInternet']}, {$data['saleOwnWeb']}, {$data['saleOtherWeb']}, 
					{$data['saleEmarket']}, {$data['saleSocial']}, {$data['useEService']}, '{$data['tel']}', 
					'{$data['web']}', '{$data['email']}', '{$data['note']}', {$tempAdrId}, {$tempCompanyId}, {$data['status']}, 
					{$data['ecoActivity']}, {$data['formatLaw']}, {$data['formatEconomy']}, {$tempHotelId}, {$data['tsicId']}, 
					{$tempMBId}, '{$data['operatorId']}', '{$data['dateKey']}', '{$data['editorialStaffId']}', 
					'{$data['dateKey']}', '{$data['recorderStaffId']}', '{$data['dateKey']}', '{$data['examinerId']}', '{$data['dateKey']}'
		 			)";
			$query = $this->db->query($sql);
	}

	private function hotelOption($data){
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';
		(!isset($tempHotelId)) and $tempHotelId = '';

			// 1. Insert Address table.
			$sql = "INSERT INTO address(road, alley, place, aNum) VALUES ('{$data['road']}', '{$data['alley']}', '{$data['place']}', '{$data['aNum']}')";
			$query = $this->db->query($sql);
			$tempAdrId = $this->db->insert_id();

			// 2. Insert Company table.
			$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['tName']}', '{$data['fName']}', '{$data['lName']}', '{$data['facName']}')";
			$query = $this->db->query($sql);
			$tempCompanyId = $this->db->insert_id();

			// 3. Insert Hotel table.
			$sql = "INSERT INTO hotel(hType, hRoom, hMaxPrice, hMinPrice) VALUES ({$data['hType']}, {$data['hRoom']}, {$data['hMaxPrice']}, {$data['hMinPrice']})";
			$query = $this->db->query($sql);
			$tempHotelId = $this->db->insert_id();

			// 4. Insert census table.
			$sql =	"INSERT INTO census(censusId, dateKey, dateUpdate, yearCreate, regionCode, provinceCode, districtCode, cantonCode, areaCode, 
					areaNumber, eaCode, censusCode, no, otherStatus, typeActivity, otherFEName, otherFLName, RegisTypeId, personalId, 
					nationPartnerId, totalWorkers, workers, yearActivity, hospitalBed, useComputerId, useInternetId, saleOwnWeb, saleOtherWeb,
					saleEmarket, saleSocial, notSale, tel, web, email, note, AddressId, CompanyId, StatusId, ActivityId, FormatLawId,
					FormatEconomyId, HotelId, TSICId, operatorId, dateOperator, editorialStaffId, dateEditorialStaff,
					recorderStaffId, dateRecorderStaff, examinerId, dateExaminer) VALUES
		 			('{$data['censusId']}', '{$data['dateKey']}', '{$data['dateUpdate']}', '{$data['fullYear']}', '{$data['regionId']}', '{$data['provinceId']}', '{$data['districtId']}',
		 			'{$data['cantonId']}', '{$data['areaType']}', '{$data['areaId']}', '{$data['eaId']}', '{$data['censusCode']}',
					'{$data['no']}', '{$data['otherStatus']}', '{$data['typeActivity']}', '{$data['otherFormatEconomy']}',
					'{$data['otherFormatLaw']}', {$data['regisType']}, '{$data['personalId']}', {$data['nationPartner']}, 
					{$data['totalWorkers']}, {$data['workers']}, {$data['yearActivity']}, {$data['hospitalBed']},
					{$data['useComputer']}, {$data['useInternet']}, {$data['saleOwnWeb']}, {$data['saleOtherWeb']}, 
					{$data['saleEmarket']}, {$data['saleSocial']}, {$data['useEService']}, '{$data['tel']}', 
					'{$data['web']}', '{$data['email']}', '{$data['note']}', {$tempAdrId}, {$tempCompanyId}, {$data['status']}, 
					{$data['ecoActivity']}, {$data['formatLaw']}, {$data['formatEconomy']}, {$tempHotelId}, {$data['tsicId']}, 
					'{$data['operatorId']}', '{$data['dateKey']}', '{$data['editorialStaffId']}', 
					'{$data['dateKey']}', '{$data['recorderStaffId']}', '{$data['dateKey']}', '{$data['examinerId']}', '{$data['dateKey']}'
		 			)";
			$query = $this->db->query($sql);
	}

	private function mbOption($data){
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';
		(!isset($tempMBAdrId)) and $tempMBAdrId = '';
		(!isset($tempMBId)) and $tempMBId = '';

		// 1. Insert Address table.
			$sql = "INSERT INTO address(road, alley, place, aNum) VALUES ('{$data['road']}', '{$data['alley']}', '{$data['place']}', '{$data['aNum']}')";
			$query = $this->db->query($sql);
			$tempAdrId = $this->db->insert_id();

			// 2. Insert Company table.
			$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['tName']}', '{$data['fName']}', '{$data['lName']}', '{$data['facName']}')";
			$query = $this->db->query($sql);
			$tempCompanyId = $this->db->insert_id();

			// 4. Insert (1)Mainbranch table.
			$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['mbTitleName']}', '{$data['mbFName']}', '{$data['mbLName']}', '{$data['mbDeptName']}')";
			$query = $this->db->query($sql);
			$tempMBAdrId = $this->db->insert_id();

			// 5. Insert (2)Mainbranch table.
			$sql = "INSERT INTO mainbranch(road, alley, place, aNum, provinceCode, districtCode, cantonCode, CompanyId) VALUES 
					('{$data['mbRoad']}', '{$data['mbAlley']}', '{$data['mbPlace']}', '{$data['mbANum']}',
					'{$data['mbProvince']}', '{$data['mbDistrict']}', '{$data['mbCanton']}', {$tempMBAdrId})";
			$query = $this->db->query($sql);
			$tempMBId = $this->db->insert_id();

			// 6. Insert census table.
			$sql =	"INSERT INTO census(censusId, dateKey, dateUpdate, yearCreate, regionCode, provinceCode, districtCode, cantonCode, areaCode, 
					areaNumber, eaCode, censusCode, no, otherStatus, typeActivity, otherFEName, otherFLName, RegisTypeId, personalId, 
					nationPartnerId, totalWorkers, workers, yearActivity, hospitalBed, useComputerId, useInternetId, saleOwnWeb, saleOtherWeb,
					saleEmarket, saleSocial, notSale, tel, web, email, note, AddressId, CompanyId, StatusId, ActivityId, FormatLawId,
					FormatEconomyId, TSICId, MainBranchId, operatorId, dateOperator, editorialStaffId, dateEditorialStaff,
					recorderStaffId, dateRecorderStaff, examinerId, dateExaminer) VALUES
		 			('{$data['censusId']}', '{$data['dateKey']}', '{$data['dateUpdate']}', '{$data['fullYear']}', '{$data['regionId']}', '{$data['provinceId']}', '{$data['districtId']}',
		 			'{$data['cantonId']}', '{$data['areaType']}', '{$data['areaId']}', '{$data['eaId']}', '{$data['censusCode']}',
					'{$data['no']}', '{$data['otherStatus']}', '{$data['typeActivity']}', '{$data['otherFormatEconomy']}',
					'{$data['otherFormatLaw']}', {$data['regisType']}, '{$data['personalId']}', {$data['nationPartner']}, 
					{$data['totalWorkers']}, {$data['workers']}, {$data['yearActivity']}, {$data['hospitalBed']},
					{$data['useComputer']}, {$data['useInternet']}, {$data['saleOwnWeb']}, {$data['saleOtherWeb']}, 
					{$data['saleEmarket']}, {$data['saleSocial']}, {$data['useEService']}, '{$data['tel']}', 
					'{$data['web']}', '{$data['email']}', '{$data['note']}', {$tempAdrId}, {$tempCompanyId}, {$data['status']}, 
					{$data['ecoActivity']}, {$data['formatLaw']}, {$data['formatEconomy']}, {$data['tsicId']}, 
					{$tempMBId}, '{$data['operatorId']}', '{$data['dateKey']}', '{$data['editorialStaffId']}', 
					'{$data['dateKey']}', '{$data['recorderStaffId']}', '{$data['dateKey']}', '{$data['examinerId']}', '{$data['dateKey']}'
		 			)";
			$query = $this->db->query($sql);
	}

	private function defaultOption($data){
		(!isset($sql)) and $sql = '';
		(!isset($tempAdrId)) and $tempAdrId = '';
		(!isset($tempCompanyId)) and $tempCompanyId = '';

		// 1. Insert Address table.
			$sql = "INSERT INTO address(road, alley, place, aNum) VALUES ('{$data['road']}', '{$data['alley']}', '{$data['place']}', '{$data['aNum']}')";
			$query = $this->db->query($sql);
			$tempAdrId = $this->db->insert_id();

			// 2. Insert Company table.
			$sql = "INSERT INTO company(titleName, fName, lName, cName) VALUES ('{$data['tName']}', '{$data['fName']}', '{$data['lName']}', '{$data['facName']}')";
			$query = $this->db->query($sql);
			$tempCompanyId = $this->db->insert_id();

			// 6. Insert census table.
			$sql =	"INSERT INTO census(censusId, dateKey, dateUpdate, yearCreate, regionCode, provinceCode, districtCode, cantonCode, areaCode, 
					areaNumber, eaCode, censusCode, no, otherStatus, typeActivity, otherFEName, otherFLName, RegisTypeId, personalId, 
					nationPartnerId, totalWorkers, workers, yearActivity, hospitalBed, useComputerId, useInternetId, saleOwnWeb, saleOtherWeb,
					saleEmarket, saleSocial, notSale, tel, web, email, note, AddressId, CompanyId, StatusId, ActivityId, FormatLawId,
					FormatEconomyId, TSICId, operatorId, dateOperator, editorialStaffId, dateEditorialStaff,
					recorderStaffId, dateRecorderStaff, examinerId, dateExaminer) VALUES
		 			('{$data['censusId']}' , '{$data['dateKey']}' ,'{$data['dateUpdate']}' , '{$data['fullYear']}', '{$data['regionId']}', '{$data['provinceId']}', '{$data['districtId']}',
		 			'{$data['cantonId']}', '{$data['areaType']}', '{$data['areaId']}', '{$data['eaId']}', '{$data['censusCode']}',
					'{$data['no']}', '{$data['otherStatus']}', '{$data['typeActivity']}', '{$data['otherFormatEconomy']}',
					'{$data['otherFormatLaw']}', {$data['regisType']}, '{$data['personalId']}', {$data['nationPartner']}, 
					{$data['totalWorkers']}, {$data['workers']}, {$data['yearActivity']}, {$data['hospitalBed']},
					{$data['useComputer']}, {$data['useInternet']}, {$data['saleOwnWeb']}, {$data['saleOtherWeb']}, 
					{$data['saleEmarket']}, {$data['saleSocial']}, {$data['useEService']}, '{$data['tel']}', 
					'{$data['web']}', '{$data['email']}', '{$data['note']}', {$tempAdrId}, {$tempCompanyId}, {$data['status']}, 
					{$data['ecoActivity']}, {$data['formatLaw']}, {$data['formatEconomy']}, {$data['tsicId']}, 
					'{$data['operatorId']}', '{$data['dateKey']}', '{$data['editorialStaffId']}', 
					'{$data['dateKey']}', '{$data['recorderStaffId']}', '{$data['dateKey']}', '{$data['examinerId']}', '{$data['dateKey']}'
		 			)";
			$query = $this->db->query($sql);
	}

	private function primaryDelete($addrId, $comId){
		$sql = "DELETE FROM address WHERE addressId ='{$addrId}'";
		$query = $this->db->query($sql);
		$sql = "DELETE FROM company WHERE comId ='{$comId}'";
		$query = $this->db->query($sql);
	}
}