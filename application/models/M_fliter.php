<?php 
class M_fliter extends CI_Model
{

	
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('census', TRUE);
	}
	
	public function GetRegion ()
	{
		$sql = "SELECT * FROM region";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function GetGeneralProvince ()
	{
		$sql = "SELECT provinceCode, provinceName FROM province";
		$query = $this->db->query($sql);
		return $query;
	}

	public function GetProvince ($rId)
	{
		$sql = "SELECT * FROM province WHERE province.regionCode = ".$rId;
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function GetDistrict ($pId)
	{
		$sql = "SELECT * FROM district WHERE district.provinceCode = ".$pId;
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function GetCanton ($bw1, $bw2)
	{
		$sql = "SELECT * FROM canton WHERE cantonCode BETWEEN ".$bw1." AND ".$bw2;
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function GetArea ($bw1, $bw2)
	{
		$sql = 'SELECT * FROM area WHERE areaNumber BETWEEN '.$bw1.' AND '.$bw2;
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function GetVillage ($bw1, $bw2)
	{
		$sql = 'SELECT * FROM village WHERE EACode BETWEEN '.$bw1.' AND '.$bw2;
		$query = $this->db->query($sql);
		return $query;
	}
	//insert
	public function insertVillage($eaCode,$villageNum,$villageName){
		$sql = 'INSERT INTO village (EACode, villageNumber, villageName)
                VALUE (?,?,?)';
        $this->db->query($sql, array($eaCode,$villageNum,$villageName));
	}
	
	public function insertArea($acode,$anum,$aname){
		$sql = 'INSERT INTO area (areaCode, areaNumber, areaName)
                VALUE (?,?,?)';
        $this->db->query($sql, array($acode,$anum,$aname));
	}
	
	public function insertCanton($cid,$cname){
		$sql = 'INSERT INTO canton (cantonCode, cantonName)
                VALUE (?,?)';
        $this->db->query($sql, array($cid,$cname));
	}
	
}