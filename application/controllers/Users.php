<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
//header('Content-Type: application/x-www-form-urlencoded');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct () {
		 parent::__construct();
		 $this->load->model('M_users');
		 $this->load->library('encrypt');
	}
	 
	public function index()
	{
		$this->load->view('welcome_message');
		
	}
	
	public function api_user(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	// $lastYear = substr(substr(date("Y-m-d"),0,4)+543,2,3);
	// echo $lastYear;

		$data = $this->M_users->test_users();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					//$pass_decode = $this->encrypt->decode($row->password);
					// if($row->status == 1){
					// 	$status = "ทำงาน";
					// }else{
					// 	$status = "ลาออก";
					// }
					//$de = $this->encrypt->decode($row->password);

					array_push($result, array(
													'usersId' => $row->usersId,
													'userName' => $row->userName,
													'password' => $this->encrypt->decode($row->password),
													'fName' => $row->fName,
													'lName' => $row->lName,
													'status' => $row->status,
													'prefixId' => $row->prefix,
													'prefix' => $row->pName,
													'trId' => $row->trId
													
												)
						);
				}
			}
		 echo json_encode($result);
		
	}

		public function check_search(){
		$data = $this->input->post('data');
		
		//$resulted = $this->M_users->m_check_search($data);
		if($resulted > 0){
			//$this->output->set_status_header(200);
			$this->api_search($data);
		}else{
			$this->output->set_status_header(204);
		}

	}

		public function api_search(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		$data1 = $this->input->post('data');
		$data = $this->M_users->test_search($data1);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$dateNow = substr(date("Y-m-d")+543,2,2);
					$dateNowCensus = substr($row->censusCode,0,2);
					if($row->dateupdate != '0000-00-00' && $dateNow == $dateNowCensus){
						$dateupdate = '(แก้ไข)';
					}else{
						$dateupdate = '';
					}

					array_push($result, array(
													'censusId' => $row->censusId,
													'dateKey' => $row->dateKey,
													'censusCode' => $row->censusCode,
													'comName' => $row->comName,
													'aName' => $row->aName,
													'cName' => $row->cName,
													'titleName' => $row->titleName,
													'fName' => $row->fristName,
													'lName' => $row->lastName,
													'dName' => $row->dName,
													'pName' => $row->pName,
													'rName' => $row->rName,
													'actName' => $row->actName,
													'typeActivity' => $row->typeActivity,
													'road' => $row->road,
													'alley' => $row->alley,
													'place' => $row->place,
													'aNum' => $row->aNum,
													'statusName' => $row->statusName,
													'statusId' => $row->statusId,
													'dateupdate' => $dateupdate,
													'tsic' => $row->tsic,
													'tsName' => $row->tsName
													
												)
						);
				}
		}else{
			$this->output->set_status_header(204);
		}
		 echo json_encode($result);
		
	}

	public function api_user_show($id){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
	
		$data = $this->M_users->users_show($id);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					//$pass_decode = $this->encrypt->decode($row->password);
					array_push($result, array(
													'usersId' => $row->usersId,
													'userName' => $row->userName,
													'password' => $this->encrypt->decode($row->password),
													'fName' => $row->fName,
													'lName' => $row->lName,
													'status' => $row->status,
													'prefix' => $row->pName
													
												)
						);
				}
			}
		 echo json_encode($result);
		
	}

	public function api_typeUser(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
	
		$data = $this->M_users->type_user();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					//$pass_decode = $this->encrypt->decode($row->password, 'overdose');
					array_push($result, array(
													'trId' => $row->trId,
													'trName' => $row->trName
													
												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	
	public function user_del($id){
		$this->M_users->user_del($id);
		$this->api_user();

	}

	public function add_user(){
		
		$data['username'] = $this->input->post('username');
		$data['prefix'] = $this->input->post('prefix');
		$pass_encode = $this->input->post('password');	
		$data['password'] = $this->encrypt->encode($pass_encode);
		//$data['password'] = $this->input->post('password');
		$data['fname'] = $this->input->post('fname');
		$data['lname'] = $this->input->post('lname');
		$data['trId'] = $this->input->post('trId');
		if(filter_var($data['prefix'], FILTER_VALIDATE_INT) === false){
			 $data['prefix'] = $this->M_users->insert_prefix($data['prefix']);
		}
		$resulted = $this->M_users->insert_user($data);
		if($resulted === 'OK'){
			$this->output->set_status_header(200);
			$this->api_user();
		}else{
			$this->output->set_status_header(204);
		}
		
		
		
		// $this->output->set_status_header(200);
		// $this->api_user();
		// print_r($this->input->post('username'));

	}

	public function edit_user(){
		$data['userid'] = $this->input->post('userid');
		$data['prefix'] = $this->input->post('prefix');
		$data['username'] = $this->input->post('username');
		$pass_encode = $this->input->post('password');	
		$data['password'] = $this->encrypt->encode($pass_encode);
		//$data['password'] = $this->input->post('password');
		$data['fname'] = $this->input->post('fname');
		$data['lname'] = $this->input->post('lname');
		$data['status'] = $this->input->post('status');
		$data['trId'] = $this->input->post('trId');
		if(filter_var($data['prefix'], FILTER_VALIDATE_INT) === false){
			 $data['prefix'] = $this->M_users->insert_prefix($data['prefix']);
		}
		$resulted = $this->M_users->update_user($data);
		if($resulted === 'OK'){
			$this->output->set_status_header(200);
			$this->api_user();
		}else{
			$this->output->set_status_header(204);
		}
		
		// print_r($this->input->post('username'));

	}

	public function search_ADBV(){
		
		$number = $this->input->post('number');
		$name = $this->input->post('name');
		$status = $this->input->post('status');
		
		$year_key = $this->input->post('year');
		$year = substr($year_key,2,4);
		$regionId = $this->input->post('regionId');
		$provinceId = $this->input->post('provinceId');
		$districtId = $this->input->post('districtId');
		$cantonId = $this->input->post('cantonId');
		$areaType = intval($this->input->post("areaType"));
		$areaId = $this->input->post('areaId');
		$eaId = $this->input->post('eaId');
		 
		 if($provinceId == 0){
			$censusCode = $year;
		 }elseif($districtId == 0){
			$censusCode = $year.$provinceId;
		 }elseif($cantonId == 0){
			$censusCode = $year.$provinceId.$districtId;
		 }elseif($areaType == 0){
			$censusCode = $year.$provinceId.$districtId.$cantonId;
		 }elseif($areaId == 0){
			$censusCode = $year.$provinceId.$districtId.$cantonId.$areaType;
		 }else{
			$censusCode = $year.$provinceId.$districtId.$cantonId.$areaType.$eaId;
		 }

		$resulted = $this->M_users->m_check_search_ADBV($censusCode,$regionId);
		if($resulted > 0){
			//$this->output->set_status_header(200);
			$this->api_search_ADBV($censusCode,$regionId);
		}else{
			$this->output->set_status_header(204);
		}

	}

	public function api_search_ADBV(){
		
		$number = $this->input->post('number');
		$name = $this->input->post('name');
		$status = $this->input->post('status');
		
		$year_key = $this->input->post('year');
		$year = substr($year_key,2,4);
		$regionId = $this->input->post('regionId');
		$provinceId = $this->input->post('provinceId');
		$districtId = $this->input->post('districtId');
		$cantonId = $this->input->post('cantonId');
		$areaType = intval($this->input->post("areaType"));
		$areaId = $this->input->post('areaId');
		$eaId = $this->input->post('eaId');
		 
		 if($provinceId == 0){
			$censusCode = $year;
		 }elseif($districtId == 0){
			$censusCode = $year.$provinceId;
		 }elseif($cantonId == 0){
			$censusCode = $year.$provinceId.$districtId;
		 }elseif($areaType == 0){
			$censusCode = $year.$provinceId.$districtId.$cantonId;
		 }elseif($areaId == 0){
			$censusCode = $year.$provinceId.$districtId.$cantonId.$areaType;
		 }else{
			$censusCode = $year.$provinceId.$districtId.$cantonId.$areaType.$eaId;
		 }
		
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		$data = $this->M_users->m_search_ADBV($censusCode,$regionId,$number,$name,$status);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$dateNow = substr(date("Y-m-d")+543,2,2);
					$dateNowCensus = substr($row->censusCode,0,2);
					if($row->dateupdate != '0000-00-00' && $dateNow == $dateNowCensus){
						$dateupdate = '(แก้ไข)';
					}else{
						$dateupdate = '';
					}
					array_push($result, array(
													'censusId' => $row->censusId,
													'dateKey' => $row->dateKey,
													'censusCode' => $row->censusCode,
													'comName' => $row->comName,
													'aName' => $row->aName,
													'cName' => $row->cName,
													'titleName' => $row->titleName,
													'fName' => $row->fristName,
													'lName' => $row->lastName,
													'dName' => $row->dName,
													'pName' => $row->pName,
													'rName' => $row->rName,
													'actName' => $row->actName,
													'typeActivity' => $row->typeActivity,
													'road' => $row->road,
													'alley' => $row->alley,
													'place' => $row->place,
													'aNum' => $row->aNum,
													'statusName' => $row->statusName,
													'statusId' => $row->statusId,
													'dateupdate' => $dateupdate,
													'tsic' => $row->tsic,
													'tsName' => $row->tsName
												)
						);
				}
			}else{
				$this->output->set_status_header(204);
			}
		 echo json_encode($result);
		
	}

	public function searchResidueInfo() {
		
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		$year = intval(date("Y")) + 539;
		$data = $this->M_users->getResidueInfo($year);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$dateNow = substr(date("Y-m-d")+543,2,2);
					$dateNowCensus = substr($row->censusCode,0,2);
					if($row->dateupdate != '0000-00-00' && $dateNow == $dateNowCensus){
						$dateupdate = '(แก้ไข) ';
					}else{
						$dateupdate = '';
					}
					array_push($result, array(
													'censusId' => $row->censusId,
													'dateKey' => $row->dateKey,
													'censusCode' => $row->censusCode,
													'comName' => $row->comName,
													'aName' => $row->aName,
													'cName' => $row->cName,
													'titleName' => $row->titleName,
													'fName' => $row->fristName,
													'lName' => $row->lastName,
													'dName' => $row->dName,
													'pName' => $row->pName,
													'rName' => $row->rName,
													'actName' => $row->actName,
													'typeActivity' => $row->typeActivity,
													'road' => $row->road,
													'alley' => $row->alley,
													'place' => $row->place,
													'aNum' => $row->aNum,
													'statusName' => $row->statusName,
													'statusId' => $row->statusId,
													'dateupdate' => $dateupdate,
													'tsic' => $row->tsic,
													'tsName' => $row->tsName
													
												)
						);
				}
			}
		 echo json_encode($result);

	}

	public function api_prefixs(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;

		$data = $this->M_users->getPrefix();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {

					array_push($result, array(
													'prefixId' => $row->pId,
													'prefixName' => $row->pName

												)
						);
				}
			}
		 echo json_encode($result);
		
	}

}
