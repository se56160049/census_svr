<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Report extends CI_Controller {

     public function __construct () {
		 parent::__construct();
		 $this->load->model('M_report');
	}
	
	public function api_report($year){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
	
		$data = $this->M_report->GetReport1($year);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'year' => $row->year,
													'status1' => $row->status1,
													'status2' => $row->status2,
													'status3' => $row->status3,
													'status4' => $row->status4,
													'status5' => $row->status5,
													'status6' => $row->status6,
													'status7' => $row->status7,
													'status8' => $row->status8
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_report_pie($year){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
		$data = $this->M_report->GetReportPie($year);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'year' => $row->year,
													'status1' => $row->status1,
													'status2' => $row->status2,
													'status3' => $row->status3,
													'status4' => $row->status4,
													'status5' => $row->status5,
													'status6' => $row->status6,
													'status7' => $row->status7,
													'status8' => $row->status8
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_year_report(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getYear();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'year' => $row->year
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_year_reports(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getYears();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'year' => $row->year
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_province(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getProvince();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'province' => $row->province,
													'name' => $row->name,
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_district($province){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getDistrict($province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'district' => $row->district,
													'name' => $row->name,
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_canton($province,$district){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		$countStr = strlen($district);
		if($countStr == 1){
			 $districtStr = "0".$district;
		}else{
			$districtStr = $district;
		}
		$canton = $province.$districtStr;
		$data = $this->M_report->getCanton($canton);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'canton' => $row->canton,
													'name' => $row->name,
												)
						);
				}
			}
		 echo json_encode($result);
	}

	public function api_report_table($year,$yearTo,$province){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		(!isset($i)) and $i = 0;

		$data = $this->M_report->GetAllStatistics($yearTo,$province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'aName' => $row->aName,
													'num1' => $row->num1,
													'percen1' => $row->percen1
												)
						);
				}
		}
		$data = $this->M_report->GetAllStatistics($year,$province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$result[$i]['num2'] = $row->num1;
					$result[$i++]['percen2'] = $row->percen1;
				}
			}
		 echo json_encode($result);
	}

	public function api_report_table_district($year,$yearTo,$province,$district){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		(!isset($i)) and $i = 0;

		$data = $this->M_report->GetAllStatisticsDistrict($yearTo,$province,$district);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'aName' => $row->aName,
													'num1' => $row->num1,
													'percen1' => $row->percen1
												)
						);
				}
			}
		$data = $this->M_report->GetAllStatisticsDistrict($year,$province,$district);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$result[$i]['num2'] = $row->num1;
					$result[$i++]['percen2'] = $row->percen1;
				}
			}
		 echo json_encode($result);
	}

	public function api_report_table_district_canton($year,$yearTo,$province,$district,$canton){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		(!isset($i)) and $i = 0;
		$cantons = substr($canton,4,6);
		//echo $cantons;
		$data = $this->M_report->GetAllStatisticsDistrictCanton($yearTo,$province,$district,$cantons);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'aName' => $row->aName,
													'num1' => $row->num1,
													'percen1' => $row->percen1
												)
						);
				}
			}
		$data = $this->M_report->GetAllStatisticsDistrictCanton($year,$province,$district,$cantons);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$result[$i]['num2'] = $row->num1;
					$result[$i++]['percen2'] = $row->percen1;
				}
			}
		 echo json_encode($result);
	}

	public function api_report_statusTable($year,$yearTo,$province){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		(!isset($i)) and $i = 0;

		$data = $this->M_report->GetAllStatus($yearTo,$province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'aName' => $row->sName,
													'num1' => $row->num1,
													'percen1' => $row->percen1
												)
						);
				}
			}
		$data = $this->M_report->GetAllStatus($year,$province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$result[$i]['num2'] = $row->num1;
					$result[$i++]['percen2'] = $row->percen1;
				}
			}
	
		 echo json_encode($result);		 		
	}

	public function api_report_statusTable_district($year,$yearTo,$province,$district){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		(!isset($i)) and $i = 0;
		
		$data = $this->M_report->GetAllStatusDistrict($yearTo,$province,$district);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'aName' => $row->sName,
													'num1' => $row->num1,
													'percen1' => $row->percen1
												)
						);
				}
			}
		$data = $this->M_report->GetAllStatusDistrict($year,$province,$district);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$result[$i]['num2'] = $row->num1;
					$result[$i++]['percen2'] = $row->percen1;
				}
			}
		 echo json_encode($result);		 		
	}

	public function api_report_statusTable_district_canton($year,$yearTo,$province,$district,$canton){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		(!isset($i)) and $i = 0;
		
		$cantons = substr($canton,4,6);
		$data = $this->M_report->GetAllStatusDistrictCanton($yearTo,$province,$district,$cantons);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'aName' => $row->sName,
													'num1' => $row->num1,
													'percen1' => $row->percen1
												)
						);
				}
			}
		$data = $this->M_report->GetAllStatusDistrictCanton($year,$province,$district,$cantons);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					$result[$i]['num2'] = $row->num1;
					$result[$i++]['percen2'] = $row->percen1;
				}
			}
		 echo json_encode($result);		 		
	}

	public function api_countStatus($year,$yearTo,$province){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getCountStatus($year,$yearTo,$province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'status' => $row->countStatus,
													'StatusLastYear' => $row->countStatuslast,
													'pName'	=> $row->pName	
												)
						);
				}
			}
		 echo json_encode($result);		 		
	}

	public function api_countStatusD($year,$yearTo,$province,$district){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getCountStatusD($year,$yearTo,$province,$district);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'status' => $row->countStatus,
													'StatusLastYear' => $row->countStatuslast,
													'pName'	=> $row->pName,
													'dName'	=> $row->dName
												)
						);
				}
			}
		 echo json_encode($result);		 		
	}

	public function api_countStatusC($year,$yearTo,$province,$district,$canton){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$cantons = substr($canton,4,6);
		$data = $this->M_report->getCountStatusC($year,$yearTo,$province,$district,$cantons,$canton);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'status' => $row->countStatus,
													'StatusLastYear' => $row->countStatuslast,
													'pName'	=> $row->pName,
													'dName'	=> $row->dName,
													'cName'	=> $row->cName
												)
						);
				}
			}
		 echo json_encode($result);		 		
	}

	public function api_countStatistics($year,$yearTo,$province){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getCountStatistics($year,$yearTo,$province);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'status' => $row->countStatus,
													'StatusLastYear' => $row->countStatuslast,
													'pName'	=> $row->pName
												)
						);
				}
			}
		 echo json_encode($result);		 		
	}
	
	public function api_countStatisticsD($year,$yearTo,$province,$district){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_report->getCountStatisticsD($year,$yearTo,$province,$district);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'status' => $row->countStatus,
													'StatusLastYear' => $row->countStatuslast,
													'pName'	=> $row->pName,
													'dName'	=> $row->dName	
												)
						);
				}
			}
		 echo json_encode($result);		 		
	}

	public function api_countStatisticsC($year,$yearTo,$province,$district,$canton){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$cantons = substr($canton,4,6);
		$data = $this->M_report->getCountStatisticsC($year,$yearTo,$province,$district,$cantons,$canton);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'status' => $row->countStatus,
													'StatusLastYear' => $row->countStatuslast,
													'pName'	=> $row->pName,
													'dName'	=> $row->dName,
													'cName'	=> $row->cName
												)
						);
				}
			}
		 echo json_encode($result);		 		
	}

}