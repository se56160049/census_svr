<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Inputs extends CI_Controller {

	public $censusId13Digits = '';
	public $censusId15Digits = '';

    public function __construct () {
	    parent::__construct();
		$this->load->model('M_inputs');
	}

    public function getStatus() {
        (!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		 $data = $this->M_inputs->getStatus();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'statusId' => $row->statusId,
											'sName' => $row->sName
											)
							);
				}
			}
		 echo json_encode($result);
    }

	public function getActivity() {
        (!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		 $data = $this->M_inputs->getActivity();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'activityId' => $row->activityId,
											'aName' => $row->aName
											)
							);
				}
			}
		 echo json_encode($result);
    }

	public function getFormatLaw() {
        (!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		 $data = $this->M_inputs->getFormatLaw();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'flId' => $row->flId,
											'flName' => $row->flName
											)
							);
				}
			}
		 echo json_encode($result);
    }

	public function getFormatEconomy() {
        (!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		 $data = $this->M_inputs->getFormatEconomy();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'feId' => $row->feId,
											'feName' => $row->feName
											)
							);
				}
			}
		 echo json_encode($result);
    }

	public function getTSIC($acId) {
        (!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_inputs->getTSIC($acId);
		
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'tsic' => $row->tsic,
											'tsName' => $row->tsName
											)
							);
				}
			}
		 echo json_encode($result);
    }

	public function getUsers() {
        (!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_inputs->getUsers();
		
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'id' => $row->userName,
											'name' => $row->fName . ' ' . $row->lName
											)
							);
				}
			}
		 echo json_encode($result);
    }

	public function getFliterData($censusId) {
		
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_inputs->getFormCensusId($censusId);
		
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'year' => $row->yearCreate,
											'regionId' => $row->regionCode,
											'provinceId' => $row->provinceCode,
											'districtId' => $row->districtCode,
											'cantonId' => $row->cantonCode,
											'areaType' => $row->areaCode,
											'areaId' => $row->areaNumber,
											'eaId' => $row->eaCode,
										)
								);
				}
		}

		echo json_encode($result);								
	}

	public function getInputData($censusId) {

		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
	
		$data = $this->M_inputs->getFormCensusId($censusId);
		
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
								'road' => $row->road,
								'alley' => $row->alley,
								'place' => $row->place,
								'aNum' => $row->aNum,
								'tName' => $row->titleName,
								'fName' => $row->fName,
								'lName' => $row->lName,
								'facName' => $row->cName,
								'status' => $row->StatusId,
								'otherStatus' => $row->otherStatus,
								'ecoActivity' => $row->ActivityId,
								'typeActivity' => $row->typeActivity,
								'formatEconomy' => $row->FormatEconomyId,
								'otherFormatEconomy' => $row->otherFEName,
								'formatLaw' => $row->FormatLawId,
								'otherFormatLaw' => $row->otherFLName,
								'regisType' => $row->RegisTypeId,
								'personalId' => $row->personalId,
								'nationPartner' => $row->nationPartnerId,
								'totalWorkers' => $row->totalWorkers,
								'workers' => $row->workers,
								'yearActivity' => $row->yearActivity,
								'hospitalBed' => $row->hospitalBed,
								'hType' => $row->hType,
								'hRoom' => $row->hRoom,
								'hMinPrice' => $row->hMinPrice,
								'hMaxPrice' => $row->hMaxPrice,
								'useComputer' => $row->useComputerId,
								'useInternet' => $row->useInternetId,
								'saleOwnWeb' => $row->saleOwnWeb,
								'saleOtherWeb' => $row->saleOtherWeb,
								'saleEmarket' => $row->saleEmarket,
								'saleSocial' => $row->saleSocial,
								'useEService' => $row->notSale,
								'tel' => $row->tel,
								'web' => $row->web,
								'email' => $row->email,
								'tsicId' => $row->TSICId,
								'mbTitleName' => $row->mbTitleName,
								'mbFName' => $row->mbFName,
								'mbLName' => $row->mbLName,
								'mbDeptName' => $row->mbCName,
								'mbRoad' => $row->mbRoad,
								'mbAlley' => $row->mbAlley,
								'mbPlace' => $row->mbPlace,
								'mbANum' => $row->mbANum,
								'mbProvince' => $row->mbProvinceId,
								'mbDistrict' => $row->mbDistrictId,
								'mbCanton' => $row->mbCantonId,
								'note' => $row->note,
								'operatorId' => $row->operatorId,
								'editorialStaffId' => $row->editorialStaffId,
								'recorderStaffId' => $row->recorderStaffId,
								'examinerId' => $row->examinerId
								)
							);
				}
			}

		echo json_encode($result);
	}

	public function insertInputs() {

		// echo 'insert and update';
		(!isset($data)) and $data = array();
		(!isset($resulted)) and $resulted = array();

		// census data.
		$data = [
				// for generate censusId.
				'year' => substr($this->input->post('year'), -2),
				'fullYear' => $this->input->post('year'),
				'regionId' => $this->input->post('regionId'),
				'provinceId' => $this->input->post('provinceId'),
				'districtId' => $this->input->post('districtId'),
				'cantonId' => $this->input->post('cantonId'),
				'areaType' => $this->input->post('areaType'),
				'areaId' => $this->input->post('areaId'),
				'eaId' => $this->input->post('eaId'),
				'censusCode' => $this->input->post('censusCode'),
				// for general data.
					// Address table.
				'road' => $this->input->post('road'),
				'alley' => $this->input->post('alley'),
				'place' => $this->input->post('place'),
				'aNum' => $this->input->post('aNum'),
					// Company table.
				'tName' => $this->input->post('tName'),
				'fName' => $this->input->post('fName'),
				'lName' => $this->input->post('lName'),
				'facName' => $this->input->post('facName'),
					// Census table.
				'status' => intval($this->input->post('status')),
				'otherStatus' => $this->input->post('otherStatus'),

				'ecoActivity' => intval($this->input->post('ecoActivity')),
				'typeActivity' => $this->input->post('typeActivity'),
				'formatEconomy' => intval($this->input->post('formatEconomy')),
				'otherFormatEconomy' => $this->input->post('otherFormatEconomy'),
				'formatLaw' => intval($this->input->post('formatLaw')),
				'otherFormatLaw' => $this->input->post('otherFormatLaw'),
				'regisType' => intval($this->input->post('regisType')),
				'personalId' => $this->input->post('personalId'),
				'nationPartner' => intval($this->input->post('nationPartner')),
				'totalWorkers' => intval($this->input->post('totalWorkers')),
				'workers' => intval($this->input->post('workers')),
				'yearActivity' => intval($this->input->post('yearActivity')),
				'hospitalBed' => intval($this->input->post('hospitalBed')),
					// Hotel table.
				'hType' => intval($this->input->post('hType')),
				'hRoom' => intval($this->input->post('hRoom')),
				'hMinPrice' => intval($this->input->post('hMinPrice')),
				'hMaxPrice' => intval($this->input->post('hMaxPrice')),
					// Census table.
				'useComputer' => intval($this->input->post('useComputer')),
				'useInternet' => intval($this->input->post('useInternet')),
				'saleOwnWeb' => intval($this->input->post('saleOwnWeb')),
				'saleOtherWeb' => intval($this->input->post('saleOtherWeb')),
				'saleEmarket' => intval($this->input->post('saleEmarket')),
				'saleSocial' => intval($this->input->post('saleSocial')),
				'useEService' => intval($this->input->post('useEService')),
				'tel' => $this->input->post('tel'),
				'web' => $this->input->post('web'),
				'email' => $this->input->post('email'),
				'tsicId' => intval($this->input->post('tsicId')),
					// Mainbranch table.
				'mbTitleName' => $this->input->post('mbTitleName'),
				'mbFName' => $this->input->post('mbFName'),
				'mbLName' => $this->input->post('mbLName'),
				'mbDeptName' => $this->input->post('mbDeptName'),
				'mbRoad' => $this->input->post('mbRoad'),
				'mbAlley' => $this->input->post('mbAlley'),
				'mbPlace' => $this->input->post('mbPlace'),
				'mbANum' => $this->input->post('mbANum'),
				'mbProvince' => $this->input->post('mbProvince'),
				'mbDistrict' => $this->input->post('mbDistrict'),
				'mbCanton' => $this->input->post('mbCanton'),
				'note' => $this->input->post('note'),
				// Census table.
				'operatorId' => $this->input->post('operatorId'),
				'editorialStaffId' => $this->input->post('editorialStaffId'),
				'recorderStaffId' => $this->input->post('recorderStaffId'),
				'examinerId' => $this->input->post('examinerId')
		];

		$this->generateCensusId($data['year'],
								$data['provinceId'],
								$data['districtId'],
								$data['cantonId'],
								$data['areaType'],
								$data['eaId']
								);

		// Check update ?.
		// if (substr($this->censusId15Digits, 0, -3) === substr($data['censusCode'], 0, -3))
			// $data['censusId'] = $data['censusCode'];
		// else
		// $data['censusId'] = $this->censusId15Digits;

		$data['dateKey'] = date((intval(date("Y")) + 543). '-m-d');
		// var_dump($data);
		if ($data['status'] === 1) {
			if ($data['censusCode'] === NULL) {
				// echo 'insert';
				$data['dateUpdate'] = '';
				$data['censusId'] = '';
				$data['censusCode'] = $this->censusId15Digits;
				$data['no'] = substr($data['censusCode'], -3);

				if ($data['ecoActivity'] == 8 && $data['formatEconomy'] == 3){
					$resulted = $this->M_inputs->insertFullInputs($data);
				} else if ($data['ecoActivity'] == 8 && $data['formatEconomy'] != 3) {
					$resulted = $this->M_inputs->insertHotelInputs($data);
				} else if ($data['ecoActivity'] != 8 && $data['formatEconomy'] == 3) {
					$resulted = $this->M_inputs->insertMainBranchInputs($data);
				} else {
					$resulted = $this->M_inputs->insertDefaultInputs($data);
				}
			} else {
				// echo 'update';
				$data['dateUpdate'] = date((intval(date("Y")) + 543). '-m-d');
				$optionData = $this->M_inputs->getOptionData($data['censusCode']);
				if ($optionData->num_rows() > 0) {
					foreach ($optionData->result() as $row) {

						$data['dateKey'] = $row->dateKey;
						$data['censusId'] = $row->censusId;
						// Check update ?.
						if (substr($this->censusId15Digits, 0, -3) != substr($data['censusCode'], 0, -3))
							$data['censusCode'] = $this->censusId15Digits;
		
						$data['no'] = substr($data['censusCode'], -3);
						
						if ($row->HotelId === NULL and $row->MainBranchId === NULL) {
							$resulted = $this->M_inputs->updateFromDefaultInputs($data, $row->AddressId, $row->CompanyId);
						} else if ($row->HotelId === NULL and $row->MainBranchId !== NULL) {
							$resulted = $this->M_inputs->updateFromMBInputs($data, $row->AddressId, $row->CompanyId,
																$row->mbComId, $row->MainBranchId);
						} else if ($row->HotelId !== NULL and $row->MainBranchId === NULL) {
							$resulted = $this->M_inputs->updateFromHotelOption($data, $row->AddressId, $row->CompanyId,
																$row->HotelId);
						} else {
							$resulted = $this->M_inputs->updateFromFullInputs($data, $row->AddressId, $row->CompanyId,
																$row->HotelId, $row->mbComId, $row->MainBranchId);
						}	

					}
				}
			}
			// status > 1
		} else { 
			// insert status > 2
			if ($data['censusCode'] === NULL) {
				$data['dateUpdate'] = '';
				$data['censusId'] = '';
				$data['censusCode'] = $this->censusId15Digits;
				$data['no'] = substr($data['censusCode'], -3);
				// status = 2
				if ($data['status'] === 2){
					$resulted = $this->M_inputs->insertStatuseq2Inputs($data);
				// status > 2
				} else {
					$resulted = $this->M_inputs->insertStatusge2Inputs($data);
				}
			} else {
			// update status > 2 
				$data['dateUpdate'] = date((intval(date("Y")) + 543). '-m-d');
				$optionData = $this->M_inputs->getOptionData($data['censusCode']);
				if ($optionData->num_rows() > 0) {
					foreach ($optionData->result() as $row) {

						$data['dateKey'] = $row->dateKey;
						$data['censusId'] = $row->censusId;
						// Check update ?.
						if (substr($this->censusId15Digits, 0, -3) != substr($data['censusCode'], 0, -3))
							$data['censusCode'] = $this->censusId15Digits;
		
						$data['no'] = substr($data['censusCode'], -3);
						
						if ($row->HotelId === NULL and $row->MainBranchId === NULL) {
							$resulted = $this->M_inputs->updateFromge2DefaultInputs($data, $row->AddressId, $row->CompanyId);
						} else if ($row->HotelId === NULL and $row->MainBranchId !== NULL) {
							$resulted = $this->M_inputs->updateFromge2MBInputs($data, $row->AddressId, $row->CompanyId,
																$row->mbComId, $row->MainBranchId);
						} else if ($row->HotelId !== NULL and $row->MainBranchId === NULL) {
							$resulted = $this->M_inputs->updateFromge2HotelOption($data, $row->AddressId, $row->CompanyId,
																$row->HotelId);
						} else {
							$resulted = $this->M_inputs->updateFromge2FullInputs($data, $row->AddressId, $row->CompanyId,
																$row->HotelId, $row->mbComId, $row->MainBranchId);
						}	
					}
				}
			}
		}
		// var_dump($resulted);
		if($resulted === 'ok'){
			return $this->output->set_status_header(200);
		} else {
			return $this->output->set_status_header(204);
		}
	}

	private function generateCensusId(... $arg) {

		foreach ($arg as $key => $value) {
				$this->censusId13Digits .= $value;
			}

		(!isset($data)) and $data = NULL;
		
		$data = $this->M_inputs->checkCensusId($this->censusId13Digits);

		if ($data->num_rows() > 0) {
			foreach ($data->result() as $row) {

				// First No.
				if ($row->No === NULL) {
					$this->censusId15Digits = $this->censusId13Digits . '001';
				}
				// Not First No.
				$this->censusId15Digits =  $this->censusId13Digits . sprintf("%'.03d", (intval($row->No)+1));
			}
		}
	}
	
}
