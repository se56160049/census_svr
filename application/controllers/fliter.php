<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Fliter extends CI_Controller {

	 public function __construct () {
		 parent::__construct();
		 $this->load->model('M_fliter');
	}
	 
	
	public function api_fliter_region(){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
	
		$data = $this->M_fliter->GetRegion();
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'regionId' => $row->regionId,
													'regionCode' => $row->regionCode,
													'regionName' => $row->regionName
												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	
	public function api_fliter_province($rid){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
		
		$data = $this->M_fliter->GetProvince($rid);
		
		
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					
					array_push($result, array(
													'provinceId' => $row->provinceId,
													'provinceCode' => sprintf('%02d', $row->provinceCode),
													'provinceName' => $row->provinceName
												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	
	public function api_fliter_district($pid){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
	
		$data = $this->M_fliter->GetDistrict($pid);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'districtId' => $row->districtId,
													'districtCode' => sprintf('%02d', $row->districtCode),
													'districtName' => $row->districtName

												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	
	public function api_fliter_canton($pid,$rid){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
		$bw1 = sprintf('%02d', $pid).sprintf('%02d', $rid).'00';
		$bw2 = sprintf('%02d', $pid).sprintf('%02d', $rid).'99';
	
		$data = $this->M_fliter->GetCanton($bw1,$bw2);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'cantonId' => $row->cantonId,
													'cantonCode' => substr($row->cantonCode,4,2),
													'cantonName' => $row->cantonName

												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	
	public function api_fliter_area($pid,$did,$cid,$aid){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
		$bw1 = sprintf('%02d', $pid).sprintf('%02d', $did).sprintf('%02d', $cid).$aid.'000';
		$bw2 = sprintf('%02d', $pid).sprintf('%02d', $did).sprintf('%02d', $cid).$aid.'999';
		
		$data = $this->M_fliter->GetArea($bw1,$bw2);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
					
													'areaId' => $row->areaId,
													'areaCode' => substr($row->areaNumber,7,3),
													'areaName' => $row->areaName
													
												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	
	public function api_fliter_village($pid,$did,$cid,$atid,$aid){
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
		$bw1 = sprintf('%02d', $pid).sprintf('%02d', $did).sprintf('%02d', $cid).$atid.sprintf('%03d', $aid).'000';
		$bw2 = sprintf('%02d', $pid).sprintf('%02d', $did).sprintf('%02d', $cid).$atid.sprintf('%03d', $aid).'999';
		
		$data = $this->M_fliter->GetVillage($bw1,$bw2);
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
													'villageId' => $row->villageId,
													'EACode' => substr($row->EACode,10,3),
													'villageNumber' => $row->villageNumber,
													'villageName' => $row->villageName

												)
						);
				}
			}
		 echo json_encode($result);
		
	}
	//input area and villageId
	
	public function addArea(){
		$acode = $this->input->post('acode');
		$anum = $this->input->post('anum');
		$name = $this->input->post('aname');

		$this->M_fliter->insertArea($acode,$anum,$name);
		$this->output->set_status_header(200);
	}
	
	public function addVillage(){
		$eaCode = $this->input->post('eaCode');
		$villageNum = $this->input->post('vnum');
		$villageName = $this->input->post('vname');

		$this->M_fliter->insertVillage($eaCode,$villageNum,$villageName);
		$this->output->set_status_header(200);
	}
	
	//auto insert
	
	public function area(){
		
		$json = file_get_contents('\xampp\htdocs\census_svr\application\controllers\aa.json');
		$Data = json_decode($json, true);
		//print_r($Data);
		foreach ($Data as $rows => $index) {
		
			foreach ($index as $a ) {
			//print_r($index);
			$acode = intval(substr($a['pid'],6,1));
			$anum = $a['pid'];
			$name =  $a['name'];
			//echo $acode.' '.$anum.' '.$name.' '.$cid.'<br>';
			$this->M_fliter->insertArea($acode,$anum,$name);
			}
		}
		//echo "OK";
	}
	
	public function tumbon(){
		
		$json = file_get_contents('\xampp\htdocs\census_svr\application\controllers\th.json');
		$Data = json_decode($json, true);
		//print_r($Data);
		foreach ($Data as $rows => $index) {
		
			foreach ($index as $a ) {
			//print_r($index);
			$code = $a['pid'];
			$name = $a['name'];
			

			$this->M_fliter->insertCanton($code,$name);
			}
		}
		//echo "OK";
	}

	public function getGeneralProvince() {
		(!isset($result)) and $result = array();
		(!isset($data)) and $data = null;
		
		$data = $this->M_fliter->GetGeneralProvince();
		
		if ($data->num_rows() > 0) {
				foreach ($data->result() as $row) {
					array_push($result, array(
											'provinceCode' => sprintf('%02d', $row->provinceCode),
											'provinceName' => $row->provinceName
										)
								);
				}	
			}
		 echo json_encode($result);
	}
}
