<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Auth extends CI_Controller {

     public function __construct () {
		 parent::__construct();
         $this->load->library('session');
		 $this->load->library('encrypt');
		 $this->load->model('M_users');
	}

    public function login(){

        //$username = "pee3177";
        //$pass = "admin";
		
		$data = $this->M_users->check_login($this->input->post('username')); 

		if($data->num_rows() > 0){
			$datauser = $data->row();
			
			 if($this->encrypt->decode($datauser->password) === $this->input->post('password')){
				 //echo 'pass true';
				 $this->output->set_status_header(200);
				 
				if($this->session->tempdata($this->input->post('username') .'User') != $this->input->post('username')){

					$newdata = array(
							$this->input->post('username') .'User' => $this->input->post('username'),
							$this->input->post('username') .'Token' => $this->getToken(rand(6,9)),
							$this->input->post('username') .'Datetime' => date("F j, Y, G:i:s a")
					);
					$this->session->set_tempdata($newdata, NULL, 14400);
					
					echo json_encode(array('token' => $this->session->tempdata( $this->input->post('username') .'Token'), 'permission' => $this->convertPermission($datauser->trId)));
				}else {
					echo 'already token.';
					//echo json_encode($this->session->tempdata( $this->input->post('username') .'Token'));
					echo json_encode(array('token' => $this->session->tempdata( $this->input->post('username') .'Token'), 'permission' => 1));
				}
				 
			}else{
				//echo 'pass false';
				$this->output->set_status_header(204);
			}
		}else{
			//echo 'no user name';
			$this->output->set_status_header(204);
		}

      
    }

    private function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        
        for ($i=0; $i < $length; $i++) {
            $token .= openssl_random_pseudo_bytes($i);
        }

        return bin2hex($token);
    }
	
	private function convertPermission ($trId){
		if ($trId > 0 && $trId < 5){
			return 2;
		} elseif($trId == 0) {
			return 3;
		}
	}
	
	public function encoding(){
		echo $this->encrypt->encode('pee123');
	}
	
	

}